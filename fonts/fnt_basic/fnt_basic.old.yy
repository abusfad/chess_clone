{
    "id": "4f3fd82f-9a8e-48ef-aa0b-3943edcde551",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_basic",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f60522a8-db45-438a-9f35-e21d6d6bbed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 172,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d046105f-1580-473a-bd69-2c9fce1727aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 172,
                "offset": 12,
                "shift": 41,
                "w": 18,
                "x": 465,
                "y": 350
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "131fd469-f37e-4180-bb05-7d180c5866e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 172,
                "offset": 6,
                "shift": 53,
                "w": 40,
                "x": 423,
                "y": 350
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "82395eb9-93c6-4292-9e1f-29a32d17c31f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 172,
                "offset": 1,
                "shift": 83,
                "w": 80,
                "x": 341,
                "y": 350
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4ae318e3-1291-426e-89cc-6343933502d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 71,
                "x": 268,
                "y": 350
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1163bfdd-4faf-40a6-86a4-9e2bac485853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 172,
                "offset": 8,
                "shift": 132,
                "w": 116,
                "x": 150,
                "y": 350
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6e43d770-8922-489a-8e85-3be7acf62620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 172,
                "offset": 6,
                "shift": 99,
                "w": 90,
                "x": 58,
                "y": 350
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3004ffe4-0272-43bd-ae79-8b613611eafc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 172,
                "offset": 6,
                "shift": 28,
                "w": 16,
                "x": 40,
                "y": 350
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b0a3d0a2-a1de-46be-b034-9155c4da93e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 172,
                "offset": 9,
                "shift": 50,
                "w": 36,
                "x": 2,
                "y": 350
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6494fca3-2c51-4448-b02d-fe46fc40e8e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 172,
                "offset": 9,
                "shift": 50,
                "w": 36,
                "x": 2007,
                "y": 176
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "eb54d2bb-c75f-4823-a01c-3f7832b38c1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 172,
                "offset": 4,
                "shift": 58,
                "w": 49,
                "x": 485,
                "y": 350
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "eec4b9e0-f16e-4ac2-92ae-fca939a7b746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 172,
                "offset": 8,
                "shift": 87,
                "w": 71,
                "x": 1934,
                "y": 176
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "38d372fd-a900-4e13-9974-4189bd19a5b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 172,
                "offset": 12,
                "shift": 41,
                "w": 17,
                "x": 1842,
                "y": 176
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "53623024-4e0e-4ab7-a8fd-b8f62f06c873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 172,
                "offset": 4,
                "shift": 50,
                "w": 41,
                "x": 1799,
                "y": 176
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8080c0b9-e756-4f0c-980f-6268a0112497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 172,
                "offset": 13,
                "shift": 41,
                "w": 16,
                "x": 1781,
                "y": 176
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8d780778-b6c3-41ef-8bee-f76a87c8c39e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 172,
                "offset": 0,
                "shift": 41,
                "w": 42,
                "x": 1737,
                "y": 176
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2d6091fd-035d-4b91-a73b-3ab464325c57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 70,
                "x": 1665,
                "y": 176
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a66a519a-db73-4d38-a68b-60b882bc6d58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 172,
                "offset": 16,
                "shift": 83,
                "w": 40,
                "x": 1623,
                "y": 176
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "94121ea8-82c2-4a64-aa77-f927d639c269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 172,
                "offset": 4,
                "shift": 83,
                "w": 72,
                "x": 1549,
                "y": 176
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e603c682-cb67-475f-a297-a8a360a983a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 71,
                "x": 1476,
                "y": 176
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f87d1d1e-a7f3-4c32-82b8-89604dcd9e63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 172,
                "offset": 1,
                "shift": 83,
                "w": 75,
                "x": 1399,
                "y": 176
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a1d7b8af-1440-4666-bbef-637aa855a985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 71,
                "x": 1861,
                "y": 176
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "72ed17cd-92d7-42f5-bcd6-ba6428b4e902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 72,
                "x": 623,
                "y": 350
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e7d1716e-3b3b-4ab8-8788-a4ce11255f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 172,
                "offset": 7,
                "shift": 83,
                "w": 70,
                "x": 1449,
                "y": 350
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "97d79e05-897a-484a-afd1-0333fbf72c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 71,
                "x": 697,
                "y": 350
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ea0f3b5f-4598-41dd-9aff-fce6707ff107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 71,
                "x": 185,
                "y": 524
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f69789d9-8f7c-44b3-a40e-90c33261aee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 172,
                "offset": 13,
                "shift": 41,
                "w": 16,
                "x": 167,
                "y": 524
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7230ee68-84d4-45e9-a563-f91996461eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 172,
                "offset": 12,
                "shift": 41,
                "w": 17,
                "x": 148,
                "y": 524
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b8e07140-dc3b-453a-8d2e-0ae50a0a9933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 172,
                "offset": 8,
                "shift": 87,
                "w": 71,
                "x": 75,
                "y": 524
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "84edb698-67a1-4125-898b-119727fcb541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 172,
                "offset": 8,
                "shift": 87,
                "w": 71,
                "x": 2,
                "y": 524
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ebc7f7ec-3a91-47e6-a7a4-def5f2c62a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 172,
                "offset": 8,
                "shift": 87,
                "w": 71,
                "x": 1933,
                "y": 350
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a9f8736f-ebc4-4f10-90e4-af045f5a0092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 70,
                "x": 1861,
                "y": 350
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "eae91890-5a74-4333-bf8e-80028fe14a61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 172,
                "offset": 8,
                "shift": 151,
                "w": 138,
                "x": 1721,
                "y": 350
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ec02b35d-895d-4d29-876e-505ac36b428f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 172,
                "offset": -1,
                "shift": 99,
                "w": 101,
                "x": 1618,
                "y": 350
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "702ef458-9bab-4089-80dd-4b038251b596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 172,
                "offset": 10,
                "shift": 99,
                "w": 82,
                "x": 258,
                "y": 524
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "064e4336-7823-46f5-9628-d3a6e46f1fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 172,
                "offset": 7,
                "shift": 108,
                "w": 95,
                "x": 1521,
                "y": 350
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b256fa37-0593-4cfa-af1c-94f68e3535fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 89,
                "x": 1358,
                "y": 350
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9b4386b7-1458-4efa-abbe-5b804e2c919d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 172,
                "offset": 11,
                "shift": 99,
                "w": 81,
                "x": 1275,
                "y": 350
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "212a084d-fe84-4632-a1cf-bb68d5900a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 172,
                "offset": 12,
                "shift": 91,
                "w": 73,
                "x": 1200,
                "y": 350
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "73868d1e-cd86-49c4-aa81-766ada6fde84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 172,
                "offset": 7,
                "shift": 116,
                "w": 100,
                "x": 1098,
                "y": 350
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "44632cda-fcd6-494c-9791-d67455505712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 85,
                "x": 1011,
                "y": 350
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "eab5846f-ee40-45f4-841f-350f11118687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 172,
                "offset": 13,
                "shift": 41,
                "w": 16,
                "x": 993,
                "y": 350
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "00afa47d-99a4-423c-80f1-013dc6ff0508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 172,
                "offset": 4,
                "shift": 75,
                "w": 59,
                "x": 932,
                "y": 350
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "06673b23-b473-4cf2-a145-70e2522e9f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 172,
                "offset": 10,
                "shift": 99,
                "w": 90,
                "x": 840,
                "y": 350
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a895ce68-a9e0-4e6d-bf99-ec14255b49bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 172,
                "offset": 10,
                "shift": 83,
                "w": 68,
                "x": 770,
                "y": 350
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "22c7bebb-7b58-4a29-9b1f-6d37c791befe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 172,
                "offset": 11,
                "shift": 124,
                "w": 102,
                "x": 1295,
                "y": 176
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "087f94ff-3b8d-4710-a270-2b11e7454c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 85,
                "x": 536,
                "y": 350
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "59781181-8022-45fd-8636-2e55036384ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 172,
                "offset": 7,
                "shift": 116,
                "w": 103,
                "x": 1190,
                "y": 176
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "74bfe68b-379e-4b98-a1ff-6eb67d84f480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 172,
                "offset": 11,
                "shift": 99,
                "w": 82,
                "x": 1694,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2596ef7a-d79c-45b0-b4a8-8738e296a793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 172,
                "offset": 6,
                "shift": 116,
                "w": 105,
                "x": 1499,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "df54d068-d6de-471d-a434-330d9c892901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 95,
                "x": 1402,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "62889a2f-d466-4ee9-8fca-ac135763c6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 172,
                "offset": 6,
                "shift": 99,
                "w": 86,
                "x": 1314,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bd02a24b-5d80-40ea-babb-c459a8d74f08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 172,
                "offset": 3,
                "shift": 91,
                "w": 86,
                "x": 1226,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "88faae81-f0f4-424f-a4ba-bf8b8da6dbfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 85,
                "x": 1139,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3d2b3b39-37d1-4672-901d-76ba072c1007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 172,
                "offset": 0,
                "shift": 99,
                "w": 99,
                "x": 1038,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "85efcbeb-dd6e-4dd7-b022-1ef327cdfeea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 172,
                "offset": 1,
                "shift": 141,
                "w": 138,
                "x": 898,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e18e2e18-36a8-4058-81be-623c22799bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 172,
                "offset": 0,
                "shift": 99,
                "w": 99,
                "x": 797,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bc24eba5-d7d9-4385-9a75-e2623dc00097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 172,
                "offset": 0,
                "shift": 99,
                "w": 99,
                "x": 696,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a0a39d46-589f-4d70-b923-dcd0e3e4f57a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 172,
                "offset": 2,
                "shift": 91,
                "w": 86,
                "x": 1606,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "083ac6ae-4522-467b-aa4d-ee265977fc1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 172,
                "offset": 10,
                "shift": 41,
                "w": 29,
                "x": 665,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d1341081-f368-4e61-afc7-4069bd651c9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 172,
                "offset": 0,
                "shift": 41,
                "w": 42,
                "x": 547,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5e7a771f-e547-412b-9a30-47651148694a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 172,
                "offset": 2,
                "shift": 41,
                "w": 30,
                "x": 515,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e42c6093-9595-41e0-8066-48f17d84fa12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 172,
                "offset": 3,
                "shift": 70,
                "w": 63,
                "x": 450,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "551b88f4-4844-45cb-8f2e-2fba152002e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 172,
                "offset": -3,
                "shift": 83,
                "w": 88,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a795a25c-4963-4d9b-9b2b-dd82bfa94fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 172,
                "offset": 6,
                "shift": 50,
                "w": 28,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "07a04a27-a61b-413a-821b-42664222e749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 72,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "04091a91-7ae7-411a-915a-f343a27d2d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 68,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5c20b136-2483-4d3b-81ff-8675c0b337da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 172,
                "offset": 5,
                "shift": 75,
                "w": 69,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4daf5ffe-ee59-4f32-9770-12dafc664408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 68,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c197cfae-4946-4af5-9187-56490f389120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 72,
                "x": 591,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "cc7b9996-a1c1-42d6-97e8-e08dc77b8c4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 172,
                "offset": 1,
                "shift": 41,
                "w": 46,
                "x": 1778,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c9140480-c817-4efa-9633-8a5c1de8b49f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 172,
                "offset": 4,
                "shift": 83,
                "w": 69,
                "x": 441,
                "y": 176
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "335a7cc9-109c-47d6-9668-795d929d989a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 64,
                "x": 1826,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "316ff065-adef-463d-a22b-8c91be61fdc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 172,
                "offset": 9,
                "shift": 33,
                "w": 14,
                "x": 1084,
                "y": 176
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "73ebde52-8b18-4c23-8c4c-a85be8cfc9cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 172,
                "offset": -7,
                "shift": 33,
                "w": 30,
                "x": 1052,
                "y": 176
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "131e7fe7-a413-4296-8b60-a809ce2a1649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 172,
                "offset": 9,
                "shift": 75,
                "w": 65,
                "x": 985,
                "y": 176
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "43d6e7b7-f10e-46c9-b85f-6b6be764e883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 172,
                "offset": 9,
                "shift": 33,
                "w": 14,
                "x": 969,
                "y": 176
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d14ae5ee-2188-4b32-ac1c-bf744d87e4da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 172,
                "offset": 9,
                "shift": 124,
                "w": 106,
                "x": 861,
                "y": 176
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e2082a5f-e751-430c-b21c-56ad0d530aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 64,
                "x": 795,
                "y": 176
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b608be43-6dbb-4393-be44-1aa1cd0d9101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 172,
                "offset": 4,
                "shift": 83,
                "w": 74,
                "x": 719,
                "y": 176
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a8e19dac-0b96-43ca-8e36-1cc370ec993a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 68,
                "x": 649,
                "y": 176
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "50686e36-a2fd-4c2e-94cf-6e7837b7cae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 68,
                "x": 579,
                "y": 176
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "68bbd052-ef7d-4c27-b21a-ed59c7f92323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 172,
                "offset": 9,
                "shift": 50,
                "w": 43,
                "x": 1100,
                "y": 176
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3404a3be-fd7a-4806-817c-b94defe853d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 172,
                "offset": 4,
                "shift": 75,
                "w": 65,
                "x": 512,
                "y": 176
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a8a5e243-eaa5-4d4f-a652-6476a774ce75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 172,
                "offset": 2,
                "shift": 41,
                "w": 39,
                "x": 400,
                "y": 176
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3a0ee922-ec75-4ed5-bbe0-7398b7adce95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 64,
                "x": 334,
                "y": 176
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a6fd33cb-4cf2-44fd-aea9-a8266d4ad257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 172,
                "offset": 1,
                "shift": 75,
                "w": 72,
                "x": 260,
                "y": 176
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "699d2499-2da3-4e52-8f9a-7526b8cc017d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 172,
                "offset": 0,
                "shift": 108,
                "w": 107,
                "x": 151,
                "y": 176
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fa7cf444-9ad0-4dbc-ac44-85f2166ab2dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 172,
                "offset": 1,
                "shift": 75,
                "w": 73,
                "x": 76,
                "y": 176
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1fa8d7c9-3d68-470d-8892-966b05d002e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 172,
                "offset": 2,
                "shift": 75,
                "w": 72,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "de47d0a5-a37c-431f-9efe-4f4211d97977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 172,
                "offset": 2,
                "shift": 75,
                "w": 70,
                "x": 1952,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5fa75393-ce66-4b32-94d7-51fa22e6ab25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 172,
                "offset": 4,
                "shift": 50,
                "w": 43,
                "x": 1907,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "dc0dccc8-d7f2-4d67-bcf1-0168d64b4046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 172,
                "offset": 13,
                "shift": 39,
                "w": 13,
                "x": 1892,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f60678ad-1f3b-41fd-81f9-16f3e9ffef78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 172,
                "offset": 3,
                "shift": 50,
                "w": 43,
                "x": 1145,
                "y": 176
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b4c27041-8d18-48ff-9d19-88511eddf86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 172,
                "offset": 6,
                "shift": 87,
                "w": 75,
                "x": 342,
                "y": 524
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "4681c49a-c587-4e12-ad17-b2cd03c430ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 65
        },
        {
            "id": "c0eadc79-c9e4-4a64-bc0b-9f37b0483894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 84
        },
        {
            "id": "6ba6259d-418f-4ae9-b754-99ba2f7c021e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 89
        },
        {
            "id": "61d14880-54da-41d9-adc1-e063e419efd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 902
        },
        {
            "id": "1cdbf680-504b-41b0-b058-d3ddf54f3d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 913
        },
        {
            "id": "df62273b-1a90-43e6-982c-cfe3f6aba19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 916
        },
        {
            "id": "bc6f690e-3565-4835-a5a9-fbe733fbe678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 923
        },
        {
            "id": "0eeed69a-b011-4269-aec4-7fe0bb043003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 932
        },
        {
            "id": "714caf0f-830e-41aa-a4e8-916728b10798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 933
        },
        {
            "id": "f812782a-1871-442f-ad62-512a950c44b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 939
        },
        {
            "id": "e7064e1a-262f-4c6c-81bb-f764090e994d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 49,
            "second": 49
        },
        {
            "id": "efd03a6e-306a-40eb-99ec-afec74864b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 32
        },
        {
            "id": "48d1a6ee-eeba-4ecd-931c-38bdeaa513a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 84
        },
        {
            "id": "1dfa5480-2832-4229-b373-1f0acd55d591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 86
        },
        {
            "id": "f18dea41-fead-47ed-a135-8eae8b58c18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 87
        },
        {
            "id": "c4976272-299d-4d58-9ccc-a609e092b63b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 89
        },
        {
            "id": "13a255d7-9f4b-4132-86ef-1a1c9832aaae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 118
        },
        {
            "id": "dfa73f59-f4b6-4d10-9984-6d692120a52a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 119
        },
        {
            "id": "d1672b7f-cf70-420f-8bef-f1b1fe1bda4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 121
        },
        {
            "id": "d8f36224-a66b-44c3-9c75-5e05fae9f3a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 160
        },
        {
            "id": "c48c8e2f-fa84-48d1-9591-065748a6e70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 8217
        },
        {
            "id": "05c199ff-06b0-4ba3-9fcd-0f2a69141ba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 70,
            "second": 44
        },
        {
            "id": "9a153188-bbd8-46c2-99f3-219105d1c8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 70,
            "second": 46
        },
        {
            "id": "6fef74a5-ff3c-4da9-9474-78e83e0030d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 65
        },
        {
            "id": "4b2ef1ee-00b9-475a-9154-dbd7f0cb27b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 32
        },
        {
            "id": "fe73d77e-25d3-4895-bc3f-3a515473316b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 84
        },
        {
            "id": "53c3280a-8777-4fcd-95c4-fac5f428654e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 86
        },
        {
            "id": "7ed8a507-fdd1-4faa-b0d5-d3b3fe898199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 87
        },
        {
            "id": "c469cd14-d923-4262-821d-c060c51355b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 89
        },
        {
            "id": "ed2c51b0-a255-493d-9d41-1c03bd0cf127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 121
        },
        {
            "id": "6a915803-4202-47a8-ab03-6740bc77afeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 160
        },
        {
            "id": "d3142a98-7fa5-4b30-b12e-eb0e9d0027e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 8217
        },
        {
            "id": "b4f2301b-8af2-419c-bfa4-ec174366ddb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 32
        },
        {
            "id": "c06a3952-d47a-49be-9c74-dc6adc126b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 80,
            "second": 44
        },
        {
            "id": "43def547-145c-47ea-bcde-4a3e7f143115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 80,
            "second": 46
        },
        {
            "id": "172168f2-a983-4717-8f25-20e941918ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 80,
            "second": 65
        },
        {
            "id": "c8a94fc8-0b90-48b0-a8d2-e3776c9df8b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 160
        },
        {
            "id": "f09b9374-be88-4fa5-8eaf-91d88879f6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 84
        },
        {
            "id": "7e6637fd-e0ef-459b-a800-4f42bf22f005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 86
        },
        {
            "id": "1c1fd4c4-ac8d-40ce-ad1f-2e7dd1241e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 87
        },
        {
            "id": "555448e7-eb4b-4b69-b83a-b8bc04540e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 89
        },
        {
            "id": "671979e6-4ba8-4156-9da1-a83458053ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 32
        },
        {
            "id": "b7c7f4be-9d9f-4cd8-86ab-401202c28ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 44
        },
        {
            "id": "700814de-953c-4674-91ff-17201826169e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 45
        },
        {
            "id": "506650f4-68f2-4ca0-86d3-4341574d0cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 46
        },
        {
            "id": "fdc4c8f1-fcc8-442b-b44b-4eb22591cadb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 58
        },
        {
            "id": "db18ee35-a99a-4ff2-8dc1-2a1a0ad571cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 59
        },
        {
            "id": "6aee9e87-6c51-4568-862a-e23bdbe97a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 84,
            "second": 65
        },
        {
            "id": "153717e1-2f31-4a6f-af89-e48202f73844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 79
        },
        {
            "id": "a19f5c2d-a67e-4a5b-8f6d-05c91c9e0290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 97
        },
        {
            "id": "8f241976-0514-4676-b198-f3d3c84d945d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 99
        },
        {
            "id": "ed0fda69-c580-4a83-90e8-8a92a2502760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 101
        },
        {
            "id": "f7090172-a618-4614-9d68-2c815d6a5067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 105
        },
        {
            "id": "2a0dfeed-8051-4d3d-bc2d-b595d70898ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 111
        },
        {
            "id": "f00b773b-0332-45d3-ab2b-550d637e1c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 114
        },
        {
            "id": "fa76f27f-4284-4c4c-b582-078f92074fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 115
        },
        {
            "id": "b9e3aeb5-0935-4511-9cc7-efa1b38581ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 117
        },
        {
            "id": "97a31ee1-4b0f-4b99-be3d-129cf2b733e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 119
        },
        {
            "id": "dd5a6423-5e0d-4042-9678-d26d2e581f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 121
        },
        {
            "id": "b7bb8abb-3f18-485e-94e6-108132114e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 160
        },
        {
            "id": "9163f63e-18a0-41c7-a5d1-04c24bf07cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 173
        },
        {
            "id": "129eca72-b001-4f46-b72c-3bf26a44a2e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 894
        },
        {
            "id": "0541ea71-7061-41a8-b16b-08160524e01c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 86,
            "second": 44
        },
        {
            "id": "a7cb51dc-0c40-46c3-86a3-c36d936ea0f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 45
        },
        {
            "id": "b8aeea1c-a723-410a-a7e9-c7e8e01adb4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 86,
            "second": 46
        },
        {
            "id": "e9152b49-3427-4963-bf44-d3435620544f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 58
        },
        {
            "id": "27d85ea4-9a70-4de4-9082-e040370921a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 59
        },
        {
            "id": "6dd58649-804f-40ed-b6c3-ed102bb94d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 65
        },
        {
            "id": "f6f72898-fe6d-4742-8cc4-2e787a915cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 97
        },
        {
            "id": "f03d42bb-0e93-4c45-946c-cc69c4c59e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 101
        },
        {
            "id": "c9c77cad-a70b-427c-906a-8b9828a295aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 105
        },
        {
            "id": "0cb11fb7-0d62-45ea-92e9-61f32f210d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 111
        },
        {
            "id": "6d1137eb-b436-4171-a801-fbc4490f1274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 114
        },
        {
            "id": "e5dbf089-c5a1-48a0-962e-0a53fa6f8d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 117
        },
        {
            "id": "164ebc24-1ca7-4f2d-836a-6792bc8dedbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 121
        },
        {
            "id": "34e0ec5a-8415-4f54-beb3-a6baf2d7b054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 173
        },
        {
            "id": "1f6a57d0-77aa-4548-a4a3-dd330891a9be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 894
        },
        {
            "id": "340f56e0-35f7-4d6e-8e28-3f7d12b64ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 44
        },
        {
            "id": "7682cca7-40b3-4624-81ce-cc8fe532d717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 45
        },
        {
            "id": "0c251bcb-2788-42c6-86e8-e9848f87b6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 46
        },
        {
            "id": "c7c2214c-4509-4981-9db4-2027dd6ae888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 58
        },
        {
            "id": "c41b18d9-f503-4b5a-89f8-5c61cec6ff14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 59
        },
        {
            "id": "f5de6847-3d23-4c7e-a876-9cb11f852fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 65
        },
        {
            "id": "fd271d67-25f6-4b4e-b432-43c3751b858f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 97
        },
        {
            "id": "280dde5a-768d-47d1-9ba2-c5906ccbb3e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 101
        },
        {
            "id": "2eb3bc81-753d-477d-9aa0-b44f259ad5e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 111
        },
        {
            "id": "de03db6d-4dd5-459e-89cf-113288276621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 114
        },
        {
            "id": "df8ab6a2-9b59-4b6b-af5a-ffe45185e508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 117
        },
        {
            "id": "28e9ffe4-1542-4d31-b599-d1d2d3c6e3c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "ca40153f-6079-42eb-9356-57a7333d25b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 173
        },
        {
            "id": "e337a5d7-5986-499b-92d9-e8f9b6c617e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 894
        },
        {
            "id": "14f806d3-fcc2-499c-b179-11a7e5560bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 32
        },
        {
            "id": "5c2d663e-9e45-4083-bc70-b4a652d82c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 89,
            "second": 44
        },
        {
            "id": "0393fd94-3f69-4a49-bb57-3b4465343ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 45
        },
        {
            "id": "1696a17c-a3d5-43ef-b501-b56279de5400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 89,
            "second": 46
        },
        {
            "id": "983d650b-e0f3-4fb2-8e64-2c8d209c7d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 58
        },
        {
            "id": "57f96635-e2c5-4236-bf44-f66c358ee377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 59
        },
        {
            "id": "bf352f82-d59e-4da9-8fa5-a6155019be7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 65
        },
        {
            "id": "daad0e7b-cca7-4bfd-afcf-f2e53ddfde0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 97
        },
        {
            "id": "b5c3a094-88b8-4b75-895e-82d20f393b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 101
        },
        {
            "id": "025c3b2b-0ce0-4f55-9c17-cb9b84f26b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 105
        },
        {
            "id": "3e7c51d5-5ea4-45ca-92f9-3d70ed3f55c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 111
        },
        {
            "id": "9375393d-9a36-413b-97f1-00be9edfa238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 112
        },
        {
            "id": "912457cf-c978-42d7-8e4e-07638193da97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 113
        },
        {
            "id": "6aecf34d-35c3-4e4b-8fca-2ebe31e74ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 117
        },
        {
            "id": "ada393f3-061a-4596-9576-ee116540ccd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 118
        },
        {
            "id": "9d9fe526-8595-437c-8ab1-8e670927a9e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 160
        },
        {
            "id": "4a6c6427-32d3-4d43-8196-3398c14990b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 173
        },
        {
            "id": "e8e43e71-224b-4f7a-aa95-9c9f34354668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 894
        },
        {
            "id": "e6f4b6c1-69c4-4651-b5bf-1763db927944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 102
        },
        {
            "id": "32fc3df1-812c-474a-8316-0a65c99ceaba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 8217
        },
        {
            "id": "38dbb16e-08fd-4b19-84ea-2c570ba0f2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 114,
            "second": 44
        },
        {
            "id": "74c0bc4e-de05-4db6-9efc-0968b029f026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 114,
            "second": 46
        },
        {
            "id": "155e2d2e-349b-4c57-98e1-90fa9eaa18b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 114,
            "second": 8217
        },
        {
            "id": "ee930cbf-72ae-48d9-9cf8-02eb1387d169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 44
        },
        {
            "id": "a3315978-86af-4132-8a53-3146d1189992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 46
        },
        {
            "id": "5e6dacd9-6db6-46bc-b60b-72ce0281bad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 44
        },
        {
            "id": "84500a1c-4cd1-4afa-bf55-280e030a2721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 46
        },
        {
            "id": "f8b98e61-6800-4ae0-a926-125f5607c462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 44
        },
        {
            "id": "c7b61f88-3b2f-43b8-b9ea-eed560732389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 112,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}