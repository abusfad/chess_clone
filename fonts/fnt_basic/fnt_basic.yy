{
    "id": "4f3fd82f-9a8e-48ef-aa0b-3943edcde551",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_basic",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "275d7ea6-7630-433c-a0de-f33de70f351d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 172,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "aa3e37c9-490d-438a-a6c5-6ca13d73dd2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 172,
                "offset": 12,
                "shift": 41,
                "w": 18,
                "x": 465,
                "y": 350
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a8809181-34a5-4833-84d2-3db26fb77020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 172,
                "offset": 6,
                "shift": 53,
                "w": 40,
                "x": 423,
                "y": 350
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "65c15de8-f9ac-4a55-85d2-42ccde547c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 172,
                "offset": 1,
                "shift": 83,
                "w": 80,
                "x": 341,
                "y": 350
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "504d7404-aede-4b04-8291-4728a5528470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 71,
                "x": 268,
                "y": 350
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "09f133a2-8831-4f0d-876d-003a01da35ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 172,
                "offset": 8,
                "shift": 132,
                "w": 116,
                "x": 150,
                "y": 350
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d19e14ef-a298-44cb-80b1-f9dfe0c3c7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 172,
                "offset": 6,
                "shift": 99,
                "w": 90,
                "x": 58,
                "y": 350
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a5b31c2e-320e-43df-bb7f-b8f60778d9cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 172,
                "offset": 6,
                "shift": 28,
                "w": 16,
                "x": 40,
                "y": 350
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ec9a2150-3734-440c-b402-b44f12eaf1f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 172,
                "offset": 9,
                "shift": 50,
                "w": 36,
                "x": 2,
                "y": 350
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "bd960b37-b886-4717-9336-0cf430a97594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 172,
                "offset": 9,
                "shift": 50,
                "w": 36,
                "x": 2007,
                "y": 176
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4859cba4-4a14-42e3-bf8f-b17e9b5e7f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 172,
                "offset": 4,
                "shift": 58,
                "w": 49,
                "x": 485,
                "y": 350
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7cc8fe6f-439f-4bf4-af1d-ebac108fd251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 172,
                "offset": 8,
                "shift": 87,
                "w": 71,
                "x": 1934,
                "y": 176
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dc91e550-12cb-4ad6-b8d5-1dd2338cc743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 172,
                "offset": 12,
                "shift": 41,
                "w": 17,
                "x": 1842,
                "y": 176
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c5cedbe9-e8b8-40d0-adda-2e4a2ef01fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 172,
                "offset": 4,
                "shift": 50,
                "w": 41,
                "x": 1799,
                "y": 176
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b263c08e-f50e-440f-a1cd-f5c8ffea4852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 172,
                "offset": 13,
                "shift": 41,
                "w": 16,
                "x": 1781,
                "y": 176
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d9da49ae-99e5-4119-9c20-9df76062d898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 172,
                "offset": 0,
                "shift": 41,
                "w": 42,
                "x": 1737,
                "y": 176
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "35c47da6-757c-4937-a9c7-6e899891e8c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 70,
                "x": 1665,
                "y": 176
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b173ae30-3f41-4627-8b1c-aec3ef4c2dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 172,
                "offset": 16,
                "shift": 83,
                "w": 40,
                "x": 1623,
                "y": 176
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2a2dacd1-4ac9-4459-8604-1d59bfea4656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 172,
                "offset": 4,
                "shift": 83,
                "w": 72,
                "x": 1549,
                "y": 176
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3a548ce9-52c0-432a-80f8-c6391c175889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 71,
                "x": 1476,
                "y": 176
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f03d6f93-22ea-437d-b77c-9176e99e92a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 172,
                "offset": 1,
                "shift": 83,
                "w": 75,
                "x": 1399,
                "y": 176
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7f4e4144-0096-46bb-9ba9-c1befffbf823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 71,
                "x": 1861,
                "y": 176
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a7169576-c9b1-4a03-adb8-1db4f1f9bbe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 72,
                "x": 623,
                "y": 350
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "de6ee77c-04a6-499f-9370-0ebc935d6bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 172,
                "offset": 7,
                "shift": 83,
                "w": 70,
                "x": 1449,
                "y": 350
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "773a7eda-17ad-47bc-b924-d6f2f8f13c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 71,
                "x": 697,
                "y": 350
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "830a17ab-ddc2-4789-9f9b-186f4c2eb672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 71,
                "x": 185,
                "y": 524
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "61f37daa-3ab0-4f94-b200-12b457392051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 172,
                "offset": 13,
                "shift": 41,
                "w": 16,
                "x": 167,
                "y": 524
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6dcfb1b5-44f5-43a6-84c7-8a15c466f107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 172,
                "offset": 12,
                "shift": 41,
                "w": 17,
                "x": 148,
                "y": 524
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dae6d2eb-2e6b-40b3-9731-242c332e5310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 172,
                "offset": 8,
                "shift": 87,
                "w": 71,
                "x": 75,
                "y": 524
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d9a34b41-f703-4e43-9c7b-6a719b979163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 172,
                "offset": 8,
                "shift": 87,
                "w": 71,
                "x": 2,
                "y": 524
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "143d8f8b-d4e6-4c0a-a925-51f1b42decd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 172,
                "offset": 8,
                "shift": 87,
                "w": 71,
                "x": 1933,
                "y": 350
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "09e4b2db-16f9-415c-bf89-4226038af387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 172,
                "offset": 6,
                "shift": 83,
                "w": 70,
                "x": 1861,
                "y": 350
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5b9d9fc9-1db2-4bfe-933a-fa8690059e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 172,
                "offset": 8,
                "shift": 151,
                "w": 138,
                "x": 1721,
                "y": 350
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d5224b65-1d9e-4506-bd34-99cb7a192eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 172,
                "offset": -1,
                "shift": 99,
                "w": 101,
                "x": 1618,
                "y": 350
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e86e6982-ac44-48f2-9fc8-5184613319e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 172,
                "offset": 10,
                "shift": 99,
                "w": 82,
                "x": 258,
                "y": 524
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "986753dc-13cf-4388-b94b-5b65f57f71a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 172,
                "offset": 7,
                "shift": 108,
                "w": 95,
                "x": 1521,
                "y": 350
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "12757ac9-1216-42ce-b5b0-b143b35fdf8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 89,
                "x": 1358,
                "y": 350
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c0d70839-c418-4120-8775-cf9adea4d0ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 172,
                "offset": 11,
                "shift": 99,
                "w": 81,
                "x": 1275,
                "y": 350
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8fec154b-3b0b-4700-9c11-84aaef0cc824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 172,
                "offset": 12,
                "shift": 91,
                "w": 73,
                "x": 1200,
                "y": 350
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "aac20f9f-fcfb-4f6f-a718-f472b0357dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 172,
                "offset": 7,
                "shift": 116,
                "w": 100,
                "x": 1098,
                "y": 350
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "330390d9-7aa1-44d3-ad30-1ee9dfa9e294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 85,
                "x": 1011,
                "y": 350
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2b0d8a59-66ec-4397-899e-53d335b3a5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 172,
                "offset": 13,
                "shift": 41,
                "w": 16,
                "x": 993,
                "y": 350
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b3783b1e-0ffd-44c6-8538-217b3fb0583d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 172,
                "offset": 4,
                "shift": 75,
                "w": 59,
                "x": 932,
                "y": 350
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bb10e076-1a75-473e-aa70-b73072673819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 172,
                "offset": 10,
                "shift": 99,
                "w": 90,
                "x": 840,
                "y": 350
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2986986c-a202-4e23-9eb9-3afc012815e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 172,
                "offset": 10,
                "shift": 83,
                "w": 68,
                "x": 770,
                "y": 350
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a7b1ad13-57d1-4bd1-a7af-d44fa209a091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 172,
                "offset": 11,
                "shift": 124,
                "w": 102,
                "x": 1295,
                "y": 176
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "85fcfb24-25bb-4d4c-9c6b-85ca2e40a595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 85,
                "x": 536,
                "y": 350
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "af4b247d-7309-4577-a79b-281d33bdbc31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 172,
                "offset": 7,
                "shift": 116,
                "w": 103,
                "x": 1190,
                "y": 176
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "620c78a9-6ca0-4e1d-9ef0-af19c972ebc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 172,
                "offset": 11,
                "shift": 99,
                "w": 82,
                "x": 1694,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "41dfb188-eb04-4010-8610-e58bdbe5c3e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 172,
                "offset": 6,
                "shift": 116,
                "w": 105,
                "x": 1499,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "60ce9b6d-877f-4912-9dbd-b0b687b733c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 95,
                "x": 1402,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8736bfb4-2114-44a3-83f0-3052869efba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 172,
                "offset": 6,
                "shift": 99,
                "w": 86,
                "x": 1314,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0c94a3ba-ac2e-4c4d-a166-e2b91d1b9349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 172,
                "offset": 3,
                "shift": 91,
                "w": 86,
                "x": 1226,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "95a73210-3ff8-4f60-9ef7-55b7bcde235b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 172,
                "offset": 11,
                "shift": 108,
                "w": 85,
                "x": 1139,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3792d51a-0799-4618-8a26-9da458b58909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 172,
                "offset": 0,
                "shift": 99,
                "w": 99,
                "x": 1038,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "47f570eb-2d5c-48ff-8e05-04bbb6244bda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 172,
                "offset": 1,
                "shift": 141,
                "w": 138,
                "x": 898,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2e236037-6def-4b23-b488-b1dfeaed1261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 172,
                "offset": 0,
                "shift": 99,
                "w": 99,
                "x": 797,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1f7a2f5b-59a5-4f55-b5f7-a39c967099c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 172,
                "offset": 0,
                "shift": 99,
                "w": 99,
                "x": 696,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a56017f1-0d46-4ef0-88c2-43ea85a5c94e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 172,
                "offset": 2,
                "shift": 91,
                "w": 86,
                "x": 1606,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8f3acd4f-2364-474e-b36c-fff6b53196db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 172,
                "offset": 10,
                "shift": 41,
                "w": 29,
                "x": 665,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1e3a2fbd-7d43-4948-b849-b353ca82398b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 172,
                "offset": 0,
                "shift": 41,
                "w": 42,
                "x": 547,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "322ef5fa-955b-4ba4-8bcd-df2b05ce23a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 172,
                "offset": 2,
                "shift": 41,
                "w": 30,
                "x": 515,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4117cda7-4a8a-4ece-8b3d-3a3cde0e04fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 172,
                "offset": 3,
                "shift": 70,
                "w": 63,
                "x": 450,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bf74afb0-2255-4193-826f-8a28498cc756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 172,
                "offset": -3,
                "shift": 83,
                "w": 88,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "49e6190a-1b78-43d9-b3ed-2244534deb52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 172,
                "offset": 6,
                "shift": 50,
                "w": 28,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "40017a0e-6655-476c-82e3-925cd6c1c81c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 72,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0703db1f-b6f3-43e9-bc52-c99600e9f74c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 68,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b68ca344-6d2a-46b0-b8b2-5d7f25bca936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 172,
                "offset": 5,
                "shift": 75,
                "w": 69,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "181ac500-04f4-452d-9cf3-f96315a36c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 68,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "74084238-6687-449f-834e-c6673b8e921d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 72,
                "x": 591,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e3f0c400-cf18-4ae2-8c23-19c4a75d6431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 172,
                "offset": 1,
                "shift": 41,
                "w": 46,
                "x": 1778,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "098a5cdf-160b-4e6a-8325-6d962a287f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 172,
                "offset": 4,
                "shift": 83,
                "w": 69,
                "x": 441,
                "y": 176
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "acaad548-63b2-48a7-9afc-e4c53caf2cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 64,
                "x": 1826,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3589b617-6d1b-4443-a07c-e9d20c0509bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 172,
                "offset": 9,
                "shift": 33,
                "w": 14,
                "x": 1084,
                "y": 176
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e925b689-5ea5-4787-bde4-d4ecc30e24d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 172,
                "offset": -7,
                "shift": 33,
                "w": 30,
                "x": 1052,
                "y": 176
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cf5ac47d-65bd-425c-8111-2e0062538337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 172,
                "offset": 9,
                "shift": 75,
                "w": 65,
                "x": 985,
                "y": 176
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "20a8c2dc-41ef-4603-b60e-780c03a61c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 172,
                "offset": 9,
                "shift": 33,
                "w": 14,
                "x": 969,
                "y": 176
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "959cee49-3c84-4852-8abb-0e93015479db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 172,
                "offset": 9,
                "shift": 124,
                "w": 106,
                "x": 861,
                "y": 176
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b25ef787-d6eb-43b6-9abc-f91bfe0375d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 64,
                "x": 795,
                "y": 176
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "56792885-3165-442f-8508-6a17aa5bd439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 172,
                "offset": 4,
                "shift": 83,
                "w": 74,
                "x": 719,
                "y": 176
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0930c5e1-f8b0-474d-847f-31bfd81863f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 68,
                "x": 649,
                "y": 176
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c1128b65-d5c1-41c5-a202-7f076d168a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 172,
                "offset": 5,
                "shift": 83,
                "w": 68,
                "x": 579,
                "y": 176
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7f938d02-c777-45bb-84b6-5537a6916805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 172,
                "offset": 9,
                "shift": 50,
                "w": 43,
                "x": 1100,
                "y": 176
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "11ebb446-799d-49b4-a72b-b73a1f4409b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 172,
                "offset": 4,
                "shift": 75,
                "w": 65,
                "x": 512,
                "y": 176
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "671c2123-af94-4d27-896f-c10ae438bc47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 172,
                "offset": 2,
                "shift": 41,
                "w": 39,
                "x": 400,
                "y": 176
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6db842b4-c51e-435a-bec5-4ddcffc6d871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 172,
                "offset": 9,
                "shift": 83,
                "w": 64,
                "x": 334,
                "y": 176
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "24ecad35-5614-4876-8a21-e2d8d9a53276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 172,
                "offset": 1,
                "shift": 75,
                "w": 72,
                "x": 260,
                "y": 176
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4893f69d-98dc-458e-a315-5a8fba3e2d10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 172,
                "offset": 0,
                "shift": 108,
                "w": 107,
                "x": 151,
                "y": 176
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a1256d8f-dfc0-4b2f-a888-3f347d5799e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 172,
                "offset": 1,
                "shift": 75,
                "w": 73,
                "x": 76,
                "y": 176
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "21d45222-68ee-434d-a765-8761b169aff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 172,
                "offset": 2,
                "shift": 75,
                "w": 72,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "bcabeef1-93ed-4af7-a579-6d945d72f2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 172,
                "offset": 2,
                "shift": 75,
                "w": 70,
                "x": 1952,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d803e50c-d7b9-4a24-b283-04a634aebbea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 172,
                "offset": 4,
                "shift": 50,
                "w": 43,
                "x": 1907,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "05728278-9b9c-430a-a8d9-4c3b0284ec48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 172,
                "offset": 13,
                "shift": 39,
                "w": 13,
                "x": 1892,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "240e616f-eea5-441a-a68e-ccff6ebb4e7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 172,
                "offset": 3,
                "shift": 50,
                "w": 43,
                "x": 1145,
                "y": 176
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6784a741-667e-430f-af28-ab174baac1f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 172,
                "offset": 6,
                "shift": 87,
                "w": 75,
                "x": 342,
                "y": 524
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "84e6bde2-d8fc-4145-b430-c654750124b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 65
        },
        {
            "id": "a4a74d9a-44b0-4a00-acf4-9f02c03c56a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 84
        },
        {
            "id": "e8ec2f13-efae-4d69-9832-6b6543a462f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 89
        },
        {
            "id": "bba55e42-8141-4c58-9bd7-42bd51e07aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 902
        },
        {
            "id": "407bdc95-890b-45ba-969b-54fba8ec6c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 913
        },
        {
            "id": "c6f745d2-63e5-4186-8de5-3cb35cd8b4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 916
        },
        {
            "id": "fe358a26-1da8-4187-a9c4-56ad462b0873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 32,
            "second": 923
        },
        {
            "id": "15b765eb-41f7-4b06-9e3b-58c85e0c507b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 932
        },
        {
            "id": "d773e391-c5c0-48b6-94b2-a72a6f6c14ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 933
        },
        {
            "id": "c9f974ff-37f0-4d4b-abba-f464d3fe9a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 939
        },
        {
            "id": "a3f96b22-5d74-41b6-8447-e1c316a70c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 49,
            "second": 49
        },
        {
            "id": "b7a46f7d-849b-4a32-8968-0858fab01339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 32
        },
        {
            "id": "2fc7647c-d799-46ff-9529-5b245b368741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 84
        },
        {
            "id": "4e535e5c-bcc1-4745-a1e3-b45374e2c14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 86
        },
        {
            "id": "f8b21938-bbec-4985-b3f5-fd30da77d387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 87
        },
        {
            "id": "513f4a5b-a1f1-415d-be5c-03b9e6902f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 89
        },
        {
            "id": "10efcb12-1582-4475-ac82-c529642009cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 118
        },
        {
            "id": "16bb0cad-bcfb-402d-9586-2ccf72c04d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 119
        },
        {
            "id": "5374cfa7-ab6b-4105-8a01-72e5628d7c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 121
        },
        {
            "id": "9ef30b29-5d0e-40a7-9543-f09f7095a14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 160
        },
        {
            "id": "24039e12-630c-4c7f-83cb-da4370d4ea03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 65,
            "second": 8217
        },
        {
            "id": "8c340196-a9e0-485b-a7c6-0d56c86c41ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 70,
            "second": 44
        },
        {
            "id": "21514321-dc77-4c99-b9c2-fb6e1b73ae01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 70,
            "second": 46
        },
        {
            "id": "99938618-1369-4426-be4c-fefc669b626f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 65
        },
        {
            "id": "70b717dc-a634-4fd2-8887-ba90db01eab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 32
        },
        {
            "id": "8698ba45-576c-4c00-ada2-0657365d8c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 84
        },
        {
            "id": "6d7c1199-f895-4dd8-b098-a37a58d5c8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 86
        },
        {
            "id": "6b001071-3f01-4f5b-890b-58b6d08bd41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 87
        },
        {
            "id": "0c1c69fd-c7bf-4f5c-81c5-a7130bf73b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 89
        },
        {
            "id": "3d4aa348-a065-489a-8d1a-6b2d484b3d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 121
        },
        {
            "id": "6faa9c81-f4a6-4045-9192-2dbc96f3dede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 160
        },
        {
            "id": "739a5e42-5da9-4802-9271-130f384bd526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 8217
        },
        {
            "id": "2313ef74-597e-44e3-95b4-b1a7ddd0fbc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 32
        },
        {
            "id": "6af682d0-712a-4e24-aec4-015fb11a2c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 80,
            "second": 44
        },
        {
            "id": "1ca76edf-d388-4704-8e37-50fa12afd314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 80,
            "second": 46
        },
        {
            "id": "dabf7d85-d486-44db-a2f7-f71d8f73752f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 80,
            "second": 65
        },
        {
            "id": "dca41d12-f1ee-4131-acfb-d3345772eecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 160
        },
        {
            "id": "a463b7fd-7120-422e-bc81-f981081db598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 84
        },
        {
            "id": "e0fc9e34-381d-4d47-bb2e-d61351e38709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 86
        },
        {
            "id": "5b232812-a19f-49b1-9bf5-adb3a93c73c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 87
        },
        {
            "id": "20df4510-ce88-41a6-9b33-431fd3400238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 89
        },
        {
            "id": "aa436513-4e22-484e-8a18-27efb6a130f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 32
        },
        {
            "id": "f91ae6a5-88b0-42fd-994d-37bf27888e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 44
        },
        {
            "id": "8a29495a-67c1-4b7a-af01-d6a2d54e7033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 45
        },
        {
            "id": "471fe489-9628-4d18-a259-c14aa00ebba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 46
        },
        {
            "id": "40012392-6252-4b36-8ce7-d17b601824f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 58
        },
        {
            "id": "ae076b10-fddb-4ec9-a3f9-51e9ff833ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 59
        },
        {
            "id": "8b92ee19-aae1-4d12-a552-0486b2f3e320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 84,
            "second": 65
        },
        {
            "id": "de00f47b-1491-494c-8583-70911fa09cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 79
        },
        {
            "id": "72938fa4-3cc6-4a08-b37c-c38b2842051a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 97
        },
        {
            "id": "bbaf9760-b9d6-4384-9e9b-4aec5ef43bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 99
        },
        {
            "id": "0477b76e-dd00-4897-a973-a67d70b298aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 101
        },
        {
            "id": "20de25ad-5801-47ce-913d-910ae492a889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 105
        },
        {
            "id": "e39e4a08-37a3-4b2d-9d4d-e83bf307f544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 111
        },
        {
            "id": "bcb3e893-9692-473b-8197-b3e7ef8f9a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 114
        },
        {
            "id": "6fd5910f-5c64-431e-ab9a-75d8c76aa258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 115
        },
        {
            "id": "cbc92028-41aa-4e9c-8d56-8c6b855a6d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 117
        },
        {
            "id": "1bc3b520-1dff-41e7-b689-e1df335feb2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 119
        },
        {
            "id": "5c4dee57-e3ba-4008-9e12-99cee5bd52f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 121
        },
        {
            "id": "c2e3aa52-e5df-451e-85b9-638726204471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 160
        },
        {
            "id": "a50e0aec-c15e-4c87-9b60-62767bc2f7d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 173
        },
        {
            "id": "3245f282-b95d-413d-a921-4959619551c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 894
        },
        {
            "id": "9244aecf-af20-4a16-a47a-f38bc6477732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 86,
            "second": 44
        },
        {
            "id": "6b60191e-71ab-485c-9bc5-f2292e55a5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 45
        },
        {
            "id": "ea0794ec-b137-4a3b-902a-2f98406731ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 86,
            "second": 46
        },
        {
            "id": "76b04372-30f9-46e3-b13f-83c485849bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 58
        },
        {
            "id": "ce4d7df8-b3d3-4043-8e40-3acc51f4c6dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 59
        },
        {
            "id": "2e9d9d82-0efa-49bb-88c7-2882b9e37645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 65
        },
        {
            "id": "f0604ce7-5f8c-423a-be98-7142932a7fa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 97
        },
        {
            "id": "250d5d1a-1a26-4279-a165-38a9fd9e5fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 101
        },
        {
            "id": "89949916-3afa-41ab-ade8-edeaed9cfe07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 105
        },
        {
            "id": "6c740fac-def3-4d34-a40d-cbdeef3ad193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 111
        },
        {
            "id": "703d49ab-8bff-4918-bd46-eea0835db814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 114
        },
        {
            "id": "366269e2-bed9-4c7d-85cd-629369277f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 117
        },
        {
            "id": "3a679634-2f01-430a-b6f9-90e6fb4acb93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 121
        },
        {
            "id": "b5daea0c-ce14-4849-b356-3162e46817ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 173
        },
        {
            "id": "b8c88c0d-e075-4b16-996c-bbae0990225c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 894
        },
        {
            "id": "2d002742-7a4a-4463-a7b6-518e4d086604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 44
        },
        {
            "id": "8f4ddcb0-320c-42e0-b27a-69d968dd5abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 45
        },
        {
            "id": "2a50b498-d032-4eda-84b4-40a1c6fc8e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 46
        },
        {
            "id": "e2236565-48da-466e-be40-7ffbd704b9cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 58
        },
        {
            "id": "8567fd22-4276-4704-ac25-8d70d2947871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 59
        },
        {
            "id": "22612a75-1b4a-49c0-b9f5-2292fdd937c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 65
        },
        {
            "id": "85940ca3-395a-4125-b4db-24c8787c2800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 97
        },
        {
            "id": "dde13cfb-81cc-4381-a92d-9f2fbb0bc198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 101
        },
        {
            "id": "7d5ae7d4-6fd8-49a5-9ee1-7460f40243ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 111
        },
        {
            "id": "b1e0be53-8132-41e5-a1c5-138fa520739f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 114
        },
        {
            "id": "5bb599b4-8c60-4cb6-86c5-eea3129cc8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 117
        },
        {
            "id": "3b245b8c-1d9b-40ac-abcb-c8f1a1401d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "c264ee0f-aee0-4402-9843-ed330ce75bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 173
        },
        {
            "id": "11278a1a-6bec-4ac8-b8cc-496ead544779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 894
        },
        {
            "id": "673d7954-4b37-42ba-948d-123a8128d0b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 32
        },
        {
            "id": "bbdc2bb1-b4ff-4a15-9b79-200ebb663ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 89,
            "second": 44
        },
        {
            "id": "98bcde09-08e2-46a1-8225-4660b28fce55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 45
        },
        {
            "id": "2e550da0-7d11-43af-8b8f-e3f1b7e6d6a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 89,
            "second": 46
        },
        {
            "id": "c6dd271d-13c3-4966-baa6-fd19bee4f90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 58
        },
        {
            "id": "6f53f02e-e0f2-4541-9435-778e28cef24f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 59
        },
        {
            "id": "047994b6-ca88-463c-a29b-bf133403704b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 65
        },
        {
            "id": "5d4c55b5-4ef0-4dbb-9d69-b92db04d0fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 97
        },
        {
            "id": "db5172eb-dd9f-41df-b988-1ed152773fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 101
        },
        {
            "id": "8231fa49-ffe9-4846-b5ef-21236d967460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 105
        },
        {
            "id": "7f53280c-2f8c-41b7-863d-b3827c7dc26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 111
        },
        {
            "id": "a94e3066-10b7-4d15-b169-d28d07920262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 89,
            "second": 112
        },
        {
            "id": "ba82f768-01a8-4419-8ef4-4151e2f41e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 113
        },
        {
            "id": "5fc1b10b-52fd-485a-ad6e-b1c423a1e4ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 117
        },
        {
            "id": "4cdd901e-6fde-4dd7-8a40-36f0d2b49d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 118
        },
        {
            "id": "a26d3e53-821f-492b-93c1-42ded459b374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 160
        },
        {
            "id": "2c64f972-6847-4e40-8d5d-495840d6aa25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 89,
            "second": 173
        },
        {
            "id": "3ced84b4-fcef-44f8-bf8b-558803914b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 894
        },
        {
            "id": "1076dd51-4d4e-446c-bdf1-feaee446747d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 102
        },
        {
            "id": "bd552e75-3a84-4c8e-81c8-0cd58b95413c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 8217
        },
        {
            "id": "ae61a4c8-053f-4ba8-9dcd-c12d997b1a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 114,
            "second": 44
        },
        {
            "id": "ee2aa6ec-0f65-4d7b-95a7-31b437ff2ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 114,
            "second": 46
        },
        {
            "id": "ce31fcb6-857f-4f24-81b7-91d4ee35f8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 114,
            "second": 8217
        },
        {
            "id": "9b698363-a6bf-4ad3-823c-f3531c97c5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 44
        },
        {
            "id": "b5906c64-64db-4182-9689-60cb077a99ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 46
        },
        {
            "id": "a87bed9c-f257-4a19-b1af-cfb6558ae026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 44
        },
        {
            "id": "ed53c049-2293-4eba-b955-b27eb97a64a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 46
        },
        {
            "id": "8ae2f63d-5fb1-4886-9552-b152bacb31e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 44
        },
        {
            "id": "fe208df1-af3f-4a83-8369-3691a25b2549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 112,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}