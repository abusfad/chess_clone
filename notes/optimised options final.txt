king will check opponent options, including opponent pieces which aren't an option but he can't eat them
the rest of the pieces will check:
are they blocking the path to the king? If so, can they move and still block the path or eat the threatening
piece?
But also, is there another piece threatening the king?
In case there are 2 pieces threatening I don't think it would be possible for any piece but the king to move
at all. You can't eat one of them and block the other because the other would have blocked in the first place.
Can a single cell block 2 pieces at once? I don't think so.

So,
-V	reset threatened variables, clear blocking lists and team king threatening list (destroying contained lists)
-	For each piece on opponent team do the following:
-V	-V	Generate options list
-	-	Add to friendly blocking pieces a threatened flag
-	-	If a movement path ends in a king add that path as a list to a threatening list stored in obj_logic
		(including running piece location)
-	-	If a movement path is blocked by an opponent piece, and when continuing the path you reach the king
		list (not piercing any more pieces on the way) add that path to the blocking piece's blocking
-	options_final:
-	if there's more than one king threatening list, or if blocking has entries and there's also
	at least one king threatening list return nothing
-	if there's only one king threatening list, check if blocking variable is flagged (since there's just one list
	it would necessarily mean you're blocking on that list). If so, check through options list and rule out anything
	not on that threatening list.

I will need these lists:

Black threatening list
White threatening list