{
    "id": "26e0eab6-ae11-40d2-b13a-967fe6348b2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_sprite",
    "eventList": [
        {
            "id": "0b4de1de-ed3d-4446-a2d8-7c741e5b7aff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "26e0eab6-ae11-40d2-b13a-967fe6348b2c"
        },
        {
            "id": "8a366473-5e24-4114-b728-d49f75a9e3c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "26e0eab6-ae11-40d2-b13a-967fe6348b2c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d7522dff-97e6-4d3f-af06-1a223924c664",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}