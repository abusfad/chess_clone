///@desc cvc fast
//not using a script to maximise speed

alarm[1] = 1;
//var _start_t = current_time;
//while (current_time-_start_t<5000) //once every 5 seconds
//while (global.games<100000)
//{
//var _heatmap_values = heatmap_values;
//with (obj_piece)
//{
//	if (!disabled)
//	{
//		_heatmap_values[@ y*8+x]++;
//	}
//}
	var _pieces_list = (turn % 2 == team.black) ? black_pieces : white_pieces;

	ds_list_copy(pieces_temp, _pieces_list);
	
	_pieces_list = pieces_temp;
	
	var _possible_i = ds_list_size(_pieces_list)-1;

	var _piece,
		_options_num;
	repeat (_possible_i+1)
	{	
		var i = irandom(_possible_i);
		_piece = _pieces_list[| i];
		_options_num = ds_list_size(_piece.options_final);
		if (_options_num == 0)
		{
			ds_list_delete(_pieces_list, i);
			_possible_i--;
		}
		else
		{
			break;
		}	
	}

	var _cell = ds_list_find_value(_piece.options_final, irandom(_options_num-1));
	scr_commit_move(_piece, _cell[point.x], _cell[point.y]);
	scr_update_gameState();
//}
//else
//{
//show_message(
//"Games: "
//+ string(global.games)
//+ "\nTurns: "
//+ string(global.turns) +" ("+string(TwoDecimals(global.turns/global.games))+" Turns Per Game)"
//+ "\nStalemates: "
//+ string(global.stalemates) + " (Out of Pieces: "+string(TwoDecimals(100*global.stalemates_lack/global.games))+"%"
//								+" Out of Moves: "+string(TwoDecimals(100*global.stalemates_impasse/global.games))+"%"
//								+" Total: "+string(TwoDecimals(100*global.stalemates/global.games))+"%)"
//+ "\nWhite Wins: "
//+ string(global.whiteWins) + " ("+string(TwoDecimals(100*global.whiteWins/global.games))+"%)"
//+ "\nBlack Wins: "
//+ string(global.blackWins) + " ("+string(TwoDecimals(100*global.blackWins/global.games))+"%)"
//+ "\nCrownings: "
//+ string(global.crowning) + " ("+string(TwoDecimals(100*global.crowning/global.games))+"%)"
//+ "\nCastlings: "
//+ string(global.castlings) + " ("+string(TwoDecimals(100*global.castlings/global.games))+"%)"
//+ "\nCounter Pawns: "
//+ string(global.counterPawn) + " ("+string(TwoDecimals(100*global.counterPawn/global.games))+"%)"
//);
//}