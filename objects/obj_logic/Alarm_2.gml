///@desc cvc slow

alarm[2] = 1;

if (game_state == game_states.normal)
{
	var _focus = focus;
	if (_focus == noone)
	{
		var _pieces_list = (turn % 2 == team.black) ? black_pieces : white_pieces;

		ds_list_copy(pieces_temp, _pieces_list);
	
		_pieces_list = pieces_temp;
	
		var _possible_i = ds_list_size(_pieces_list)-1;

		var _piece,
			_options_num;
		repeat (_possible_i+1)
		{	
			var i = irandom(_possible_i);
			_piece = _pieces_list[| i];
			_options_num = ds_list_size(_piece.options_final);
			if (_options_num == 0)
			{
				ds_list_delete(_pieces_list, i);
				_possible_i--;
			}
			else
			{
				focus = _piece;
				scr_highlight_options();
				break;
			}	
		}	
	}
	else
	{
		var _options = _focus.options_final,
			_moveTo = _options[| irandom(ds_list_size(_options)-1)];
		
		scr_commit_move(_focus, _moveTo[point.x], _moveTo[point.y]);
		scr_update_gameState();
	}
}