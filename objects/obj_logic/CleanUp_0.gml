/// @description
if ds_exists(global.board, ds_type_grid) ds_grid_destroy(global.board);

if ds_exists(white_pieces, ds_type_list) ds_list_destroy(white_pieces);
if ds_exists(black_pieces, ds_type_list) ds_list_destroy(black_pieces);
if ds_exists(white_options, ds_type_list) ds_list_destroy(white_options);
if ds_exists(black_options, ds_type_list) ds_list_destroy(black_options);
if ds_exists(white_options_final, ds_type_list) ds_list_destroy(white_options_final);
if ds_exists(black_options_final, ds_type_list) ds_list_destroy(black_options_final);
if ds_exists(white_king_threatening, ds_type_list) ds_list_destroy(white_pieces);
if ds_exists(black_king_threatening, ds_type_list) ds_list_destroy(black_pieces);

if ds_exists(board_memory, ds_type_list) ds_list_destroy(board_memory);
if ds_exists(button_options, ds_type_list) ds_list_destroy(button_options);
if ds_exists(pieces_temp, ds_type_list) ds_list_destroy(pieces_temp);

if ds_exists(white_castling, ds_type_map) ds_map_destroy(white_castling);
if ds_exists(black_castling, ds_type_map) ds_map_destroy(black_castling);