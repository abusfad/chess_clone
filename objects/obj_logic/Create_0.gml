//restarted = false;
#macro LOGIC global.logic
LOGIC = id;

window_set_fullscreen(true);
randomise();

scr_globals();

#macro LAYER_PIECES LOGIC.layer_pieces
#macro LAYER_BOARD LOGIC.layer_board
#macro LAYER_UI LOGIC.layer_ui
layer_pieces = layer_tilemap_get_id("Pieces");
layer_board = layer_tilemap_get_id("Board_Tiles");
layer_ui = layer_get_id("UI");

heatmap_surf = surface_create(room_width, room_height);
heatmap_values = array_create(64, 0);
heatmap_board = shader_get_uniform(shd_heatmap, "board");
heatmap_turn = shader_get_uniform(shd_heatmap, "turn");

global.board = ds_grid_create(8, 8);

white_pieces = ds_list_create();
black_pieces = ds_list_create();

white_options = ds_list_create();
black_options = ds_list_create();

white_king_threatening = ds_list_create();
black_king_threatening = ds_list_create();

black_options_final = ds_list_create();
white_options_final = ds_list_create();

pieces_temp = ds_list_create();

board_memory = ds_list_create();

enum castling
{
	moved_king,
	moved_left,
	moved_right
}
	
black_castling = ds_map_create();
black_castling[? castling.moved_king] = false;
black_castling[? castling.moved_left] = false;
black_castling[? castling.moved_right] = false;

white_castling = ds_map_create();
white_castling[? castling.moved_king] = false;
white_castling[? castling.moved_left] = false;
white_castling[? castling.moved_right] = false;

black_pawnDouble = -2;
white_pawnDouble = -2;


focus = noone;
dragged_offset = noone;

enum team
{
	white,
	black
}


//turn increases to infinity and turn is determined using mod
turn = team.white; //-1 and not 0 because we're calling scr_update_gameState before the first turn

check = false;

current_board = undefined;

//board_memory[| turn] = scr_saveBoard(true);

focus = noone;

scr_init_board(); //some code duplication here to this event but it's just the create event

enum game_states
{
	normal,
	menu,
	finished
}

//game_state = game_states.normal;

enum game_modes
{
	pvp,
	pvc,
	cvc_slow,
	cvc_fast
}

game_mode = undefined;
player_team = team.white;

#region button options

button_options = ds_list_create();
button_active = 1;
scr_reset_buttons_list();

#endregion

global.turns = 0;
global.games = 0;
global.stalemates = 0;
global.stalemates_lack = 0;
global.stalemates_impasse = 0;
global.whiteWins = 0;
global.blackWins = 0;
global.castlings = 0;
global.counterPawn = 0;
global.crowning = 0;