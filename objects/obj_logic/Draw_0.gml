var _heatmap_surf = heatmap_surf;
if (!surface_exists(_heatmap_surf))
{
	heatmap_surf = surface_create(room_width, room_height);
	_heatmap_surf = heatmap_surf;
	surface_set_target(_heatmap_surf);
	draw_clear_alpha(c_black, 0.5);
	surface_reset_target();
}
shader_set(shd_heatmap);
shader_set_uniform_f_array(heatmap_board, heatmap_values);
shader_set_uniform_f(heatmap_turn, global.turns);
draw_surface(_heatmap_surf, 0, 0);
shader_reset();