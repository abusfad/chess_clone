if (game_mode<=game_modes.pvc) //so == to pvp or pvc
{
	if (game_state == game_states.normal)
	{
		if (game_mode==game_modes.pvp || player_team==(turn%2))
		{
			var _mouse_x = mouse_x>>TILESHIFT,
				_mouse_y = mouse_y>>TILESHIFT;
			var _piece = scr_getPiece(_mouse_x, _mouse_y);

			if (_piece != noone)
			{
				if (_piece.team != turn%2)
				{
					_piece = noone;
				}
			}

			var _focus = focus;

			if (_focus != noone)
			{
				var _options = _focus.options_final;
		
				if (findArrayInList([_mouse_x, _mouse_y], _options) != -1)
				{
					//insert code for committing move here
					scr_commit_move(_focus, _mouse_x, _mouse_y);
					_focus.dragged = false;
					_focus.depth++;
					scr_update_gameState();
				}
				else
				{
					focus = _piece; //_piece is noone if pressed on an empty cell or enemy piece
					if (_piece != noone)
					{
						_piece.dragged = true;
						_piece.depth--;
						dragged_offset = [mouse_x % TILESIZE, mouse_y % TILESIZE];
					}
					scr_highlight_options();
				}
			}
			else
			{
				focus = _piece;
				if (_piece != noone)
				{
					_piece.dragged = true;
					_piece.depth--;
					dragged_offset = [mouse_x % TILESIZE, mouse_y % TILESIZE];
				}
				scr_highlight_options();
			}
		}
	}
}