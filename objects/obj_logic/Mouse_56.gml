if (game_mode<=game_modes.pvc) //so == to pvp or pvc
{
	if (game_state == game_states.normal)
	{
		var _focus = focus;
		if (game_mode==game_modes.pvp || player_team==(turn%2))
		{
			if (_focus != noone)
			{
				if (_focus.dragged)
				{
					var _mouse_x = mouse_x>>TILESHIFT,
						_mouse_y = mouse_y>>TILESHIFT;
					var _piece = scr_getPiece(_mouse_x, _mouse_y);

					if (_piece != noone)
					{
						if (_piece.team != turn%2)
						{
							_piece = noone;
						}
					}
					var _options = _focus.options_final;
		
					if (findArrayInList([_mouse_x, _mouse_y], _options) != -1)
					{
						scr_commit_move(_focus, _mouse_x, _mouse_y);
						_focus.dragged = false;
						_focus.depth++;
						scr_update_gameState();
					}
					else
					{
						_focus.dragged = false;
						_focus.depth++;
						focus = noone;
						scr_highlight_options();
					}
				}
			}
		}
	}
}