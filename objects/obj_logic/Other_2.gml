var _width = room_height-TILESIZE,
	_height = room_width-TILESIZE,
	_x_middle = room_width/2,
	_y_middle = room_height/2;

with scr_uiBox_init(	_x_middle, _y_middle, _width, _height, fa_center, fa_center, uiStyles.old_software)
{
	alpha = 0.85;
}
with scr_button_init(scr_gamemode_pvp, [], _x_middle, _y_middle-_height*3/10, _width*3/5, _height/5,
				fa_center, fa_center, "Player vs. Player", fa_center, fa_center, uiStyles.old_software)
{
	alpha = 1;
}
with scr_button_init(scr_gamemode_pvc, [], _x_middle, _y_middle, _width*3/5, _height/5,
				fa_center, fa_center, "Player vs. Computer", fa_center, fa_center, uiStyles.old_software)
{
	alpha = 1;
}
with scr_button_init(scr_gamemode_cvc, [], _x_middle, _y_middle+_height*3/10, _width*3/5, _height/5,
				fa_center, fa_center, "Computer vs. Computer", fa_center, fa_center, uiStyles.old_software)
{
	alpha = 1;
}

game_state = game_states.menu;