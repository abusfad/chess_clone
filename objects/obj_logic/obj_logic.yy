{
    "id": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logic",
    "eventList": [
        {
            "id": "a6477906-055c-4596-8d02-706719e9ed87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "67c24007-63c3-4ccd-8a04-03baf05364db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "2c140b88-00d0-4e31-89bb-0492d802cca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "39b8d4a3-aeb0-4cf5-9104-3ceb2c836c22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "b69f33c9-e6b6-48e9-8949-6b9db704c4ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "f34bb5ea-22a2-4235-b1c4-bd0543498fc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "589384bb-0c41-4657-a664-85dc2e3d91e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "c3218893-f534-44da-9ca6-a47e1a8f05ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "8d578a75-f0a7-48d6-8e1c-77ae46a6fd67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "b139237b-81d7-4792-acac-29dc152ef4f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "993295e7-646b-497b-97c5-f8feeba4d2a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "3538054c-ac68-4429-a408-72d5e9515e21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        },
        {
            "id": "184a1b8a-01a7-4a43-8a9c-e60ff61c102e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "c45fb85d-8bae-43e8-ad39-45ae5b69b78c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}