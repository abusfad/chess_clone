if (!disabled)
{
	if (dragged)
	{
		var _off = LOGIC.dragged_offset;
		draw_sprite(sprite_index, 0, mouse_x-_off[point.x], mouse_y-_off[point.y]);
	}
	else
	{
		draw_sprite(sprite_index, 0, x*TILESIZE, y*TILESIZE);
	}
}