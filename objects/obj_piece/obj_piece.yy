{
    "id": "3adc9f74-b439-49e2-a6a0-88d5b0310e52",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_piece",
    "eventList": [
        {
            "id": "21e45741-627a-457c-ab26-fba664875f9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3adc9f74-b439-49e2-a6a0-88d5b0310e52"
        },
        {
            "id": "db905b67-c114-4dbd-b61c-72257aa4ebb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3adc9f74-b439-49e2-a6a0-88d5b0310e52"
        },
        {
            "id": "79a6913c-3aa3-47b2-b906-9d3eb1746433",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3adc9f74-b439-49e2-a6a0-88d5b0310e52"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}