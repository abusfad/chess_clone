var _time = time;

if (use_fractions)
{
	_time[@ timepart.fractions] -= 5;
	if (_time[@ timepart.fractions]<0)
	{
		_time[@ timepart.seconds]--;
		_time[@ timepart.fractions] += 100;
		if (_time[@ timepart.seconds]<timer_target)
		{
			script_execute(function);
			scr_timer_end();
			exit;
		}
	}
	alarm[0] = room_speed/20;
}
else
{
	_time[@ timepart.seconds]--;
	if (_time[@ timepart.seconds]<timer_target)
	{
		script_execute(function);
		scr_timer_end();
	}
	else alarm[0] = room_speed;
}