//var _minutes = (time[timepart.minutes]>=10) ? string(time[timepart.minutes]) : "0"+string(time[timepart.minutes]);
var _time = time,
	_negative = _time[timepart.seconds]<0,
	
	_minutes = string(_time[timepart.seconds] div 60),
	
	_seconds = abs(_time[timepart.seconds]) mod 60,
	_seconds = (_seconds>=10) ? string(_seconds) : "0"+string(_seconds);

text = (_negative) ? "-" : "";
text += _minutes+":"+_seconds;
if (use_fractions)
{
	var _fractions = _time[timepart.fractions];
	var _fractions = (_fractions>=10) ? string(_fractions) : "0"+string(_fractions);
	text += "."+_fractions;
}