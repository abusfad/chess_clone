{
    "id": "cb00d4f3-5329-4d14-89d9-bcc99c491bea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timer",
    "eventList": [
        {
            "id": "4461e4ea-435a-47de-8952-923c7bf096a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb00d4f3-5329-4d14-89d9-bcc99c491bea"
        },
        {
            "id": "c30835a7-946b-44f1-98c6-045d26af3158",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cb00d4f3-5329-4d14-89d9-bcc99c491bea"
        },
        {
            "id": "c48923a1-0f00-4895-b714-53223a57a4e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cb00d4f3-5329-4d14-89d9-bcc99c491bea"
        },
        {
            "id": "9a72fe78-cdb8-4598-92b4-f5350af23533",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cb00d4f3-5329-4d14-89d9-bcc99c491bea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84d21774-a6bd-4170-9863-ca7be73f36b5",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}