/// @description
width = 0;
height = 0;
topLeft_x = 0;
topLeft_y = 0;
outline = 0;
outline_color = c_black;
bg_color = c_white;
alpha = 1.0;

persist = false; //skipped by uibox_closeall and needs to be closed manually