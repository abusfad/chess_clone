var _outline_color = outline_color,
	_width = width,
	_height = height,
	_topLeft_x = topLeft_x,
	_topLeft_y = topLeft_y;

//inside bg color, drawn with alpha and bg_color
draw_set_alpha(alpha);
draw_set_color(bg_color);
var _outline = outline;
draw_roundrect(_topLeft_x+_outline+1, _topLeft_y+_outline+1, _topLeft_x+_width-_outline-1,
				_topLeft_y+_height-_outline-1, false);

//outline around the bg color with same alpha				
repeat(_outline)
{
	draw_roundrect_colour(_topLeft_x+_outline, _topLeft_y+_outline, _topLeft_x+_width-_outline,
							_topLeft_y+_height-_outline, _outline_color, _outline_color, true);
	_outline--;
}

draw_set_alpha(1);