#macro TEXT_OFFSET 32

event_inherited();

draw_set_font(font);
draw_set_color(text_color);
draw_set_alpha(1);
draw_set_halign(text_xalign);
draw_set_valign(text_yalign);
//draw_text(topLeft_x+width/2, topLeft_y+height/2, text);

var _width = width-2*(outline+TEXT_OFFSET);
draw_text_ext(text_x, text_y, text, -1, _width);

/*old method to scale the text to box. It works as intended but looks ugly
//scale the text by the largest int factor not getting out of bound of the box.
var _text_width = string_width_ext(_text, -1, _width),
	_text_height = string_height_ext(_text, -1, _height),
	//_scale = min((_width div _text_width), (_height div _text_height));
_scale = min((_width / _text_width), (_height / _text_height)); //non-int scaling mething, gets ugly results

draw_text_ext_transformed(_xx, _yy, _text, -1, _width/_scale, _scale, _scale, image_angle);
*/