{
    "id": "84d21774-a6bd-4170-9863-ca7be73f36b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_uiBoxText",
    "eventList": [
        {
            "id": "94112ca9-2cb4-441d-af69-da74fcbeb6d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84d21774-a6bd-4170-9863-ca7be73f36b5"
        },
        {
            "id": "a39fafca-e30b-47b6-89fa-1e2c5bdf549d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "84d21774-a6bd-4170-9863-ca7be73f36b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2ee57531-dac8-4d25-b470-5ae9727f6232",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}