///@func ArrayToList(array)
///@arg array

var _list = ds_list_create();

var i=0;
repeat(array_length_1d(argument0))
{
	ds_list_add(_list, argument0[i]);
}
return _list;