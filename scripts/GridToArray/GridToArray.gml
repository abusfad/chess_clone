///@func GridToArray(grid)
///@arg grid
var _array = [];
var _width = ds_grid_width(argument0),
	_height = ds_grid_height(argument0);
var i = _width-1,
	k = _height-1;

repeat(_height)
{
	i = _width-1;
	repeat(_width)
	{
		_array[k*_width+i] = argument0[# i, k];
		i--;
	}
	k--;
}

return _array;