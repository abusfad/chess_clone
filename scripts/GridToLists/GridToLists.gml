///@func GridToLists(grid)
///@arg grid

var _rows = ds_list_create();

var i = 0;
repeat (ds_grid_width(argument0))
{
	var _column = ds_list_create();
	var k = 0;
	repeat (ds_grid_height(argument0))
	{
		_column[| k] = argument0[# i, k];
		k++;
	}
	ds_list_add_list(_rows, _column);
	i++;
}

return _rows;