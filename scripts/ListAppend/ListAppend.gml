///@func ListAppend(list_result, list_input)
///@arg list_result
///@arg list_input

var i = 0;
repeat(ds_list_size(argument1))
{
	ds_list_add(argument0, argument1[| i++]);
}