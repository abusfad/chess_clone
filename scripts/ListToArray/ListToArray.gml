///@func ListToArray(list)
///@arg list

var _array = [];
var i=ds_list_size(argument0);
repeat(i)
{
	i--;
	_array[i] = argument0[| i];
}
return _array;