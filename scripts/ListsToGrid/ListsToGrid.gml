///@func ListsToGrid(row_list)
///@arg row_list

var _row_length = ds_list_size(argument0);
var _column_length = ds_list_size(argument0[| 0]);
var _grid = ds_grid_create(_row_length, _column_length);

var i = 0;
repeat (_row_length)
{
	var k = 0;
	repeat (_column_length)
	{
		var _column = argument0[| i];
		_grid[# i, k] = _column[| k];
		k++;
	}
	i++;
}
return _grid;