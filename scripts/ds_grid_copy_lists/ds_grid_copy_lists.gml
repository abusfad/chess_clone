///@func ds_grid_add_lists(grid, row_list)
///@arg grid
///@arg row_lists

var i = 0;
repeat (ds_list_size(argument1))
{
	var k = 0;
	repeat (ds_list_size(argument1[| 0]))
	{
		var _column = argument1[| i];
		argument0[# i, k] = _column[| k];
		k++;
	}
	i++;
}
