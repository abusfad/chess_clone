///@func ds_list_duplicate(list)
///@arg list

var _list = ds_list_create();
ds_list_copy(_list, argument0);
return _list;