///@func ds_list_add_list(id, pos, map)
///@desc ds_list_add_list(id, pos, map)
///@arg id
///@arg pos
///@arg map

ds_list_insert(argument0, argument1, argument2);
ds_list_mark_as_map(argument0, argument1);