///@func findArrayInList
///@arg array
///@arg list
var i = 0;
repeat(ds_list_size(argument1))
{
	if array_equals(argument0, argument1[| i])
	{
		return i;
	}
	i++;
}

return -1;