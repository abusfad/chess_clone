///@func scr_ExposesKing(piece, x, y)
///@arg piece
///@arg x
///@arg y

scr_commit_move(argument0, argument1, argument2);

scr_update_options();

with (obj_king)
{
	if (team==argument0.team)
	{
		var _king_x = x,
			_king_y = y,
			_team = team;
	}
}

var _exposes = scr_isOpponentOption(_king_x, _king_y, _team);

scr_loadBoard(current_board, false);

return _exposes;