///@func scr_piece_options_final(output_list)
///@arg output_list
///@arg threat_list

//each piece objects runs this on its own, uses instance variable options list, x and y

var _options = options;
var _id = id;
with LOGIC
{
	var i = 0;
	repeat (ds_list_size(_options))
	{
		var _test_cell = _options[| i];
		if (!old_ExposesKing(_id, _test_cell[point.x], _test_cell[point.y]))
		{
			ds_list_add(argument0, _test_cell);
		}
		i++;
	}
}