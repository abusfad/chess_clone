///@func scr_addPiece(obj, team, x, y)
///@arg obj
///@arg team
///@arg x
///@arg y

var _id = instance_create_layer(argument2, argument3, "Pieces", argument0);
with _id
{
	x = argument2;
	y = argument3;
	scr_update_team(_id, argument1);
}
global.board[# argument2, argument3] = _id;
var _teamList = (argument1==team.black) ? black_pieces : white_pieces;
ds_list_add(_teamList, _id);

return _id;
//pieces lists discontinued for now
//var _team_list = (argument1==team.black) ? black_pieces : white_pieces;
//ds_list_add(_team_list, _id);