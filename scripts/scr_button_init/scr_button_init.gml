///@func scr_message_init(func, args, x, y, width, height, xalign, yalign, text, text_xalign, text_yalign, style)
///@arg function
///@arg arguments_array
///@arg x
///@arg y
///@arg width
///@arg height
///@arg xalign
///@arg yalign
///@arg text
///@arg text_xalign
///@arg text_yalign
///@arg style

var _button = instance_create_layer(argument[2], argument[3], LAYER_UI, obj_button);
with (_button)
{
	depth--;
	function = argument[0];
	arguments = argument[1];
	scr_uiBoxText_settings(argument[4], argument[5], argument[6], argument[7],
							argument[8], argument[9], argument[10], argument[11]);
}
return _button;