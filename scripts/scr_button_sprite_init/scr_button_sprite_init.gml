///@func scr_message_init(func, args x, y, width, height, xalign, yalign, sprite)
///@arg function
///@arg arguments
///@arg x
///@arg y
///@arg width
///@arg height
///@arg xalign
///@arg yalign
///@arg sprite

var _button = instance_create_layer(argument[2], argument[3], LAYER_UI, obj_button_sprite);
with (_button)
{
	depth--;
	function = argument[0];
	arguments = argument[1]
	sprite_index = argument[8];
	image_xscale = argument[4]/sprite_get_width(sprite_index);
	image_yscale = argument[5]/sprite_get_height(sprite_index);
	topLeft_x = x-sprite_xoffset;
	topLeft_y = y-sprite_yoffset;
	switch argument[6]
	{
		case fa_left:
			x += sprite_get_xoffset(sprite_index)*image_xscale;
			break;
		case fa_center:
			break;
		case fa_right:
			x += sprite_get_xoffset(sprite_index)*image_xscale
				-sprite_get_width(sprite_index)*image_xscale;
			break;
	}
	switch argument[7]
	{
		case fa_top:
			y += sprite_get_yoffset(sprite_index)*image_yscale;
			break;
		case fa_center:			
			break;
		case fa_bottom:
			y += sprite_get_yoffset(sprite_index)*image_yscale
				-sprite_get_height(sprite_index)*image_yscale;
			break;
	}
}
return _button;