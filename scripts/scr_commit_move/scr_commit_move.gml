///@func scr_commit_move(piece, target_x, target_y)
///@arg piece
///@arg target_x
///@arg target_y

var _piece = argument0,
	_target_x = argument1,
	_target_y = argument2;

with (_piece)
{
	switch object_index
	{
		//castling
		case obj_rook:
			var _castling = (team==team.black) ? LOGIC.black_castling : LOGIC.white_castling;
			if orientation==orient.left
			{
				_castling[? castling.moved_left] = true;
			}
			else if orientation==orient.right
			{
				_castling[? castling.moved_right] = true;
			}
			break;
		//castling
		case obj_king:
			var _castling = (team==team.black) ? LOGIC.black_castling : LOGIC.white_castling;
			_castling[? castling.moved_king] = true;
	
			if (x-_target_x==2) //commit move assumes the move was already declared legal.
			{
				var _rook = scr_getPiece(x-4, y);
				scr_commit_move(_rook, x-1, y);
				global.castlings++;
			}
			if (x-_target_x==-2)
			{
				var _rook = scr_getPiece(x+3, y)
				scr_commit_move(_rook, x+1, y);
				global.castlings++;
			}
			break;
		//double pawn move
		case obj_pawn:
			//impasse
			if (team==team.black)
			{
				var _pawnDouble = LOGIC.white_pawnDouble,
					_crowning_y = 7,
					_counter_y = 4;
			}
			else
			{
				var _pawnDouble = LOGIC.black_pawnDouble,
					_crowning_y = 0,
					_counter_y = 3;
			}			
			
			if (_pawnDouble==_target_x && y==_counter_y)
			{
				//normal piece handling in the end of this script
				scr_removePiece(global.board[# _pawnDouble, y]);
				global.counterPawn++;
			}
			var _crowning_y = (team==team.black) ? 7 : 0;			
			if (_target_y == _crowning_y)
			{
				if (LOGIC.game_mode==game_modes.pvp
				||	LOGIC.game_mode==game_modes.pvc && team==LOGIC.player_team)
				{
					scr_crowning_menu(_target_x, _target_y, team);
					global.crowning++;
				}
				else
				{
					var _target_piece = global.board[# _target_x, _target_y];
					if (_target_piece!=noone)
					{
						scr_removePiece(_target_piece);
					}
	
					global.board[# x, y] = noone;
					global.board[# _target_x, _target_y] = _piece;
					x = _target_x;
					y = _target_y;					
					scr_crown(choose(obj_knight, obj_rook, obj_bishop, obj_queen), id);
					global.crowning++;
					LOGIC.white_pawnDouble = -2;
					LOGIC.black_pawnDouble = -2;
					exit; //originally because original piece instance destroyed, left for minor optimisation
				}
			}
				
			//setting up impasse option
			LOGIC.white_pawnDouble = -2;
			LOGIC.black_pawnDouble = -2;
			if (y-_target_y==2)
			{
				LOGIC.white_pawnDouble = x;
			}

			if (y-_target_y==-2)
			{
				LOGIC.black_pawnDouble = x;
			}
			break;
	}
	
	var _target_piece = global.board[# _target_x, _target_y];
	if (_target_piece!=noone)
	{
		scr_removePiece(_target_piece);
		LOGIC.heatmap_values[_target_y*8+_target_x]++;
	}
	
	global.board[# x, y] = noone;
	global.board[# _target_x, _target_y] = _piece;
	x = _target_x;
	y = _target_y;
}

//LOGIC.heatmap_values[_target_y*8+_target_x]++;