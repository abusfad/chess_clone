with LOGIC
{
	game_mode = argument0;
	game_state = game_states.normal;
	if (argument0==game_modes.cvc_slow)
	{
		room_speed = 2;
		alarm[2] = 1;
	}
	else if (argument0==game_modes.cvc_fast)
	{
		room_speed = 99999;
		alarm[1] = 1;
	}
}

scr_uiBox_closeAll();