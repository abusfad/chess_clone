var _turn = turn;

if (game_mode == game_modes.pvc && _turn%2 != player_team)
{
	var _team = (player_team==team.black) ? team.white : team.black,
		_pieces_list = (_team==team.black) ? black_pieces : white_pieces;

	_pieces_list = ds_list_duplicate(_pieces_list);
	
	var _possible_i = ds_list_size(_pieces_list)-1;

	var _exitLoop = false,
		_piece,
		_options_num;
	do
	{
		var i = irandom(_possible_i);
		_piece = _pieces_list[| i];
		_options_num = ds_list_size(_piece.options_final);
		if (_options_num == 0)
		{
			ds_list_delete(_pieces_list, i);
			_possible_i--;
		}
		else
		{
			_exitLoop = true;
		}	
	} until (_exitLoop)

	var _cell = ds_list_find_value(_piece.options_final, irandom(_options_num-1));
	scr_commit_move(_piece, _cell[point.x], _cell[point.y]);
	scr_update_gameState();
}