///@func scr_crown
///@arg object_index
///@arg id

var _x = argument1.x,
	_y = argument1.y,
	_team = argument1.team;
	
scr_removePiece(argument1);

with LOGIC
{
	var _crowned_piece = scr_addPiece(argument0, _team, _x, _y);
	_crowned_piece.is_crowned_pawn = true;
	
	game_state = game_states.normal;

	//if (game_mode==game_modes.pvc) scr_computer_turn();

	turn--; //needed because nothing stopped the regular scr_update_gameState
	scr_update_gameState();
}

scr_uiBox_closeAll();