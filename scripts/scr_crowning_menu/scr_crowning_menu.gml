///@func scr_crowning_menu(x, y, team)
///@arg x
///@arg y
///@arg team

var _x = argument0,
	_y = argument1,
	_id = id;

if (argument2==team.black)
{
	var _step = 1;
	var _args_rook = [spr_rook_black, obj_rook, _id];
	var _args_knight = [spr_knight_black, obj_knight, _id];
	var _args_bishop = [spr_bishop_black, obj_bishop, _id];
	var _args_queen = [spr_queen_black, obj_queen, _id];
}
else
{
	var _step = -1;
	var _args_rook = [spr_rook_white, obj_rook, _id];
	var _args_knight = [spr_knight_white, obj_knight, _id];
	var _args_bishop = [spr_bishop_white, obj_bishop, _id];
	var _args_queen = [spr_queen_white, obj_queen, _id];
}

var _left_x = clamp(_x-2, 0, 4);
var _top_y = _y-_step;
scr_uiBox_init(	_left_x*TILESIZE, _top_y*TILESIZE, 
				TILESIZE*4, TILESIZE, fa_left, fa_top, uiStyles.garden);

scr_button_sprite_init(	scr_crown, [_args_rook[1], _args_rook[2]],
						(_left_x)*TILESIZE, _top_y*TILESIZE, TILESIZE, TILESIZE,
						fa_left, fa_top, _args_rook[0]);
scr_button_sprite_init(	scr_crown, [_args_knight[1], _args_knight[2]],
						(_left_x+1)*TILESIZE, _top_y*TILESIZE, TILESIZE, TILESIZE,
						fa_left, fa_top, _args_knight[0]);
scr_button_sprite_init(	scr_crown, [_args_bishop[1], _args_bishop[2]],
						(_left_x+2)*TILESIZE, _top_y*TILESIZE, TILESIZE, TILESIZE,
						fa_left, fa_top, _args_bishop[0]);
scr_button_sprite_init(	scr_crown, [_args_queen[1], _args_queen[2]],
						(_left_x+3)*TILESIZE, _top_y*TILESIZE, TILESIZE, TILESIZE,
						fa_left, fa_top, _args_queen[0]);

LOGIC.game_state = game_states.menu;