instance_destroy(obj_button);

var _width = room_height-TILESIZE,
	_height = room_width-TILESIZE,
	_x_middle = room_width/2,
	_y_middle = room_height/2;

scr_button_init(	scr_computer_mode, [game_modes.cvc_slow], _x_middle, _y_middle-_height/5, _width*3/5, _height/5,
					fa_center, fa_center, "Slow", fa_center, fa_center, uiStyles.old_software);
scr_button_init(	scr_computer_mode, [game_modes.cvc_fast], _x_middle, _y_middle+_height/5, _width*3/5, _height/5,
					fa_center, fa_center, "Infinite", fa_center, fa_center, uiStyles.old_software);