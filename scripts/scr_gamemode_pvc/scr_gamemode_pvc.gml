LOGIC.game_mode = game_modes.pvc;

instance_destroy(obj_button);

var _width = room_height-TILESIZE,
	_height = room_width-TILESIZE,
	_x_middle = room_width/2,
	_y_middle = room_height/2;

scr_button_init(	scr_player_team, [team.white], _x_middle, _y_middle-_height/5, _width*3/5, _height/5,
					fa_center, fa_center, "White", fa_center, fa_center, uiStyles.old_software);
scr_button_init(	scr_player_team, [team.black], _x_middle, _y_middle+_height/5, _width*3/5, _height/5,
					fa_center, fa_center, "Black", fa_center, fa_center, uiStyles.old_software);