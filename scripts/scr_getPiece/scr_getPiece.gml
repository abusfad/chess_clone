///@func scr_getPiece(x, y)
///@arg x
///@arg y

var _x = argument0, _y = argument1;
	
//_x = _x>>TILESHIFT; _y = _y>>TILESHIFT;

if !scr_inBounds(_x, _y) return noone;
else return global.board[# _x, _y];