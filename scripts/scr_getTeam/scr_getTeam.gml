///@func scr_getTeam(x, y)
///@arg x
///@arg y


var _piece = global.board[# argument0, argument1];
if (_piece==noone) return noone;
else
{
	with _piece
	{
		return team;
	}
}