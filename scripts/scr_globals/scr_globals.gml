#macro TILESIZE 256
#macro TILESIZE1 255
#macro TILESHIFT 8

enum tile
{
	none,
	black,
	white,
	black_h,
	white_h,
	red,
	blue
}

enum point
{
	x,
	y
}