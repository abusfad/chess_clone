//ran by obj_logic

scr_reset_board_tiles();

var _focus = focus;
if (_focus!=noone)
{
	tilemap_set(LAYER_BOARD, tile.blue, _focus.x, _focus.y);
	var _options = _focus.options_final;

	var i = 0;
	repeat(ds_list_size(_options))
	{
		var _cell = _options[| i],
			_x = _cell[point.x],
			_y = _cell[point.y];
		var _tile = scr_tileColor(_x, _y);
		_tile = scr_tileHighlight(_tile, scr_validType(_x, _y, _focus.team));
		tilemap_set(LAYER_BOARD, _tile, _x, _y);
		i++;
	}
}