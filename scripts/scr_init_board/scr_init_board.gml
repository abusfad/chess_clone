ds_grid_clear(global.board, noone);

var _white_pieces = white_pieces;
var _black_pieces = black_pieces;
ds_list_clear(_black_pieces);
ds_list_clear(_white_pieces);

with scr_addPiece(obj_rook, team.black, 0, 0)
{
	orientation = orient.left
}
scr_addPiece(obj_knight, team.black, 1, 0);
scr_addPiece(obj_bishop, team.black, 2, 0);
scr_addPiece(obj_queen, team.black, 3, 0);
scr_addPiece(obj_king, team.black, 4, 0);
scr_addPiece(obj_bishop, team.black, 5, 0);
scr_addPiece(obj_knight, team.black, 6, 0);
with scr_addPiece(obj_rook, team.black, 7, 0)
{
	orientation = orient.right
}

var i = 0;
repeat(8)
{
	scr_addPiece(obj_pawn, team.black, 0+i, 1);
	scr_addPiece(obj_pawn, team.white, 0+i, 6);
	i++;
}

with scr_addPiece(obj_rook, team.white, 0, 7)
{
	orientation = orient.left;
}
scr_addPiece(obj_knight, team.white, 1, 7);
scr_addPiece(obj_bishop, team.white, 2, 7);
scr_addPiece(obj_queen, team.white, 3, 7);
scr_addPiece(obj_king, team.white, 4, 7);
scr_addPiece(obj_bishop, team.white, 5, 7);
scr_addPiece(obj_knight, team.white, 6, 7);
with scr_addPiece(obj_rook, team.white, 7, 7)
{
	orientation = orient.right;
}

var _black_castling = black_castling,
	_white_castling = white_castling;
_black_castling[? castling.moved_king] = false;
_black_castling[? castling.moved_left] = false;
_black_castling[? castling.moved_right] = false;
_white_castling[? castling.moved_king] = false;
_white_castling[? castling.moved_left] = false;
_white_castling[? castling.moved_right] = false;

focus = noone;
turn = team.white;
check = false;
black_pawnDouble = -2;
white_pawnDouble = -2;
scr_update_options();
scr_reset_board_tiles();