///@func scr_isEmpty(x, y)
///@arg x
///@arg y

var _piece = scr_getPiece(argument0, argument1);
return (_piece==noone);