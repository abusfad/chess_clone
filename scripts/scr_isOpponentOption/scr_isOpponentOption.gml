///@func scr_isOpponentOption(x, y, team)
///@arg x
///@arg y
///@arg team

var _opponent_team = (argument2+1)%2;
var _opponent_options = (_opponent_team=team.black) ? LOGIC.black_options : LOGIC.white_options;
var i = 0;
repeat (ds_list_size(_opponent_options))
{
	var _cell = _opponent_options[| i],
		_x = _cell[point.x],
		_y = _cell[point.y];
	if (_x==argument0 && _y == argument1) return true;
	i++;
}
return false;