///@func scr_king_options
///@arg list

var _steps =	[-1,1,	0,1,	1,1,
				-1,0,			1,0,
				-1,-1,	0,-1,	1,-1];

var _x = x,
	_y = y,
	_team = team;

var i = 0;
repeat(array_length_1d(_steps)/2)
{
	var _xx = _x+_steps[i*2];
	var _yy = _y+_steps[i*2+1];
	switch scr_validType(_xx, _yy, _team)
	{
		case valid.empty:
		case valid.opponent:
			ds_list_add(argument0, [_xx, _yy]);
			break;
		case valid.friend:
			var _piece = scr_getPiece(_xx, _yy);
			_piece.threatened = true;
			break;
	}
	i++;
}

//castling
if (scr_validType(_x-1, _y, _team)==valid.empty)
{
	if (scr_validType(_x-2, _y, _team)==valid.empty&&scr_validType(_x-3, _y, _team)==valid.empty)
	{
		ds_list_add(argument0, [_x-2, _y]);
	}
}
if (scr_validType(_x+1, _y, _team)==valid.empty)
{
	if (scr_validType(_x+2, _y, _team)==valid.empty)
	{
		ds_list_add(argument0, [_x+2, _y]);
	}
}