///@func scr_king_options_final(list, threats_list)
///@arg list
///@arg threats_list
var _team = team,
	_x = x,
	_y = y,
	_options = options,
	_options_final = argument0,
	_blocking = blocking;

var i = 0;
repeat(ds_list_size(_options))
{
	var _cell = _options[| i];
	//add option to final list if it isn't an opponent option and also isn't a piece with threatened flagged
	//and also isn't an entry in king's own blocking list
	if (!scr_isOpponentOption(_cell[point.x], _cell[point.y], _team))
	{
		if (findArrayInList(_cell, _blocking)==-1)
		{
			var _target_piece = scr_getPiece(_cell[point.x], _cell[point.y]);
			if (_target_piece != noone)
			{
				if (!_target_piece.threatened)
				{
					ds_list_add(_options_final, _cell);
				}
			}
			else
			{
				ds_list_add(_options_final, _cell);
			}
		}
	}
	i++;
}

//test if there's a check (for highlighting the tile and for castling)
with LOGIC
{
	if (_team==team.black)
	{
		var _threat = white_king_threatening,
			_castling = black_castling;
	}
	else
	{
		var _threat = black_king_threatening,
			_castling = white_castling;
	}
	var _team_check = (!ds_list_empty(_threat));
}

if (_castling[? castling.moved_king] || _team_check)
{
	//remove castling from options final
	var _castling_index = findArrayInList([_x-2, _y], _options_final);
	ds_list_delete(_options_final, _castling_index);
			
	var _castling_index = findArrayInList([_x+2, _y], _options_final);
	ds_list_delete(_options_final, _castling_index);
}
else
{
	//if moved the rook or the intermediate castling cells are not in the final options
	if (_castling[? castling.moved_left]
	||	findArrayInList([_x-1, _y], _options_final) == -1)
	{
		var _castling_index = findArrayInList([_x-2, _y], _options_final);
		ds_list_delete(_options_final, _castling_index);
	}
			
	if (_castling[? castling.moved_right]
	||	findArrayInList([_x+1, _y], _options_final) == -1)
	{
		var _castling_index = findArrayInList([_x+2, _y], _options_final);
		ds_list_delete(_options_final, _castling_index);
	}
}

if (_team==LOGIC.turn%2)
{
	LOGIC.check = _team_check;
}