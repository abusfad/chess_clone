///@func scr_knight_options
///@arg list
var _x = x, _y = y;
var _steps =	[			-1,2,			1,2,
					-2,1,							2,1,
													
					-2,-1,							2,-1,
							-1,-2,			1,-2,			];

var _team = team;
var i = 0;
repeat(array_length_1d(_steps)/2)
{
	var _xx = _x+_steps[i*2];
	var _yy = _y+_steps[i*2+1];
	var _valid = scr_validType(_xx, _yy, _team);
	switch _valid
	{
		case valid.empty:
		case valid.opponent:
			ds_list_add(argument0, [_xx, _yy]);
			break;
		case valid.friend:
			var _target = scr_getPiece(_xx, _yy);
			_target.threatened = true;
			break;
		case valid.opponent_king:
			var _threatening = ds_list_create();
			_threatening[| 0] = [_x, _y];
			ds_list_add((_team==team.black) ? LOGIC.black_king_threatening : LOGIC.white_king_threatening, _threatening);
			break;
	}
	i++;
}