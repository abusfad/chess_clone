///@func scr_loadBoard(board_map, final)
///@arg board_map
///@arg final

var _data = json_decode(argument0);
ds_grid_copy_lists(global.board, _data[? "board"]);
scr_applyBoardPositions();
	
var _stored = _data[?"white_options"];
var _white_options = white_options;
ds_list_clear(_white_options);
var i = 0;
repeat(ds_list_size(_stored))
{
	_white_options[| i] = ListToArray(_stored[| i]);
	i++;
}
	
var _stored = _data[?"black_options"];
var _black_options = black_options;
ds_list_clear(_black_options);
var i = 0;
repeat(ds_list_size(_stored))
{
	_black_options[| i] = ListToArray(_stored[| i]);
	i++;
}

if (argument1)
{
	var _stored = _data[?"white_options_final"];
	var _white_options_final = white_options_final;
	ds_list_clear(_white_options_final);
	var i = 0;
	repeat(ds_list_size(_stored))
	{
		_white_options_final[| i] = ListToArray(_stored[| i]);
		i++;
	}
		
	var _stored = _data[?"black_options_final"];
	var _black_options_final = black_options_final;
	ds_list_clear(_black_options_final);
	var i = 0;
	repeat(ds_list_size(_stored))
	{
		_black_options_final[| i] = ListToArray(_stored[| i]);
		i++;
	}
}
	
var _pieces_list = _data[? "pieces_list"];
var i = 0;
repeat(ds_list_size(_pieces_list))
{
	var _piece_map = _pieces_list[| i];
	with (_piece_map[? "id"])
	{
		var _stored = _piece_map[? "options"];
		var _options = options;
		ds_list_clear(_options);
		var j = 0;
		repeat(ds_list_size(_stored))
		{
			_options[| j] = ListToArray(_stored[| j]);
			j++;
		}
	}
	i++;
}
	
ds_map_destroy(_data);