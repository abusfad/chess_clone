///@func scr_message_init(x, y, width, height, xalign, yalign, text, text_xalign, text_yalign, style)
///@arg x
///@arg y
///@arg width
///@arg height
///@arg xalign
///@arg yalign
///@arg text
///@arg text_xalign
///@arg text_yalign
///@arg style

var _message = instance_create_layer(argument[0], argument[1], LAYER_UI, obj_message);
with (_message)
{
	scr_uiBoxText_settings(argument[2], argument[3], argument[4], argument[5],
							argument[6], argument[7], argument[8], argument[9]);
}
return _message;