///@func scr_pawn_options
///@arg list

//special case because pawns can move into certain cells but can't eat into them
//only add eating options (for king threats calculations)

var _xr = x+1,
	_xl = x-1,
	_team = team,
	_yy =  (_team==team.black) ? y+1 : y-1;

var _valid_left = scr_validType(_xl, _yy, _team);
var _valid_right = scr_validType(_xr, _yy, _team);
switch _valid_left
{
	case valid.empty: //so opponent king won't be able to move there. removed in pawn_options_final
	case valid.opponent:
		ds_list_add(argument0, [_xl, _yy]);
		break;
	case valid.opponent_king:
		ds_list_add(argument0, [_xl, _yy]);
		var _threat = ds_list_create();
		_threat[| 0] = [x, y];
		var _teamList = (_team==team.black) ? LOGIC.black_king_threatening : LOGIC.white_king_threatening;
		ds_list_add(_teamList, _threat);
		break;
	case valid.friend:
		var _piece = scr_getPiece(_xl, _yy);
		_piece.threatened = true;
		break;
}
switch _valid_right
{
	case valid.empty:
	case valid.opponent:
		ds_list_add(argument0, [_xr, _yy]);
		break;
	case valid.opponent_king:
		ds_list_add(argument0, [_xr, _yy]);
		var _threat = ds_list_create();
		_threat[| 0] = [x, y];
		var _teamList = (_team==team.black) ? LOGIC.black_king_threatening : LOGIC.white_king_threatening;
		ds_list_add(_teamList, _threat);
	case valid.friend:
		var _piece = scr_getPiece(_xr, _yy);
		_piece.threatened = true;
		break;
}
