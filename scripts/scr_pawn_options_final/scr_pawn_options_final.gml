///@func scr_pawn_options_final
///@arg list
///@arg threats

//special case because pawns can move into certain cells but can't eat into them
//first re calculate options
//then run normal scr_piece_options_final

var _options = options;
ds_list_clear(_options);

var _x = x,
	_y = y,
	_team = team;

if (_team==team.black)
{
	var _start_y = 1,
		_step = 1,
		
		_pawnDouble = LOGIC.white_pawnDouble;
}
else
{
	var _start_y = 6,
		_step = -1,
		
		_pawnDouble = LOGIC.black_pawnDouble;
}

//won't stop the king from moving to that cell as the king checks for the overall options list
//which only updates after the normal options script
//this means there's a different between the overall list and the actual sum of options list
var _valid_1 = scr_validType(_x, _y+_step, _team);
if (_valid_1==valid.empty)
{
	ds_list_add(_options, [_x, _y+_step]);
}
	
var _valid_left = scr_validType(_x-1, _y+_step, _team);
var _valid_right = scr_validType(_x+1, _y+_step, _team);
switch _valid_left
{
	//no need for all the normal logic handling as they've been done in scr_pawn_options
	case valid.opponent:
	case valid.opponent_king:
		ds_list_add(_options, [_x-1, _y+_step]);
		break;
}
switch _valid_right
{
	case valid.opponent:
	case valid.opponent_king:
		ds_list_add(_options, [_x+1, _y+_step]);
		break;
}


//first move double pawn step
if (_y==_start_y)
{
	var _valid_2 = scr_validType(_x, _y+2*_step, _team);
	if (_valid_1==valid.empty&&_valid_2==valid.empty) ds_list_add(_options, [_x, _y+2*_step]);
}

//eating an opponent double pawn step with your pawn
if (_y==_start_y+3*_step)
{
	if (_pawnDouble == _x-1)
	{
		ds_list_add(_options, [_x-1, _y+_step]);
	}
	if (_pawnDouble == _x+1)
	{
		ds_list_add(_options, [_x+1, _y+_step]);
	}
}

scr_piece_options_final(argument0, argument1);