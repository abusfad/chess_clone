///@func scr_piece_options_final(output_list, threat list)
///@arg output_list
///@arg threat_list

//each piece objects runs this on its own, uses instance variable options list, x and y
/*
-//king threatening list only include direct threats. Blocked threats go to the blocking piece's blocking list
-	if there's more than one king threatening list, or if blocking has entries and there's also
	at least one king threatening list return nothing
-	if there are no king threatening lists, check if blocking list has any entries.
	If so, check through options list and rule out anything
	not on that blocking list
*/


var _options = options,
	_blocking = blocking,
	_threats = argument1,
	_blocking_size = ds_list_size(_blocking),
	_threats_size = ds_list_size(_threats),
	_id = id;

if (_threats_size>0)
{
	if (_threats_size>=2)
	{
		return; //which results in an empty options_final list
	}
	else //threats size == 1
	{
		if (_blocking_size>0)
		{
			return;
		}
		else
		{
			//enable movement that stops the threat
			var _threat = _threats[| 0];
			var i = 0;
			repeat(ds_list_size(_options))
			{
				if (findArrayInList(_options[| i], _threat)!=-1) ds_list_add(argument0, _options[| i]);
				i++;
			}
		}
	}
}
else //no direct threats
{
	if (_blocking_size>0)
	{
		//piece is used to block a king threat and can only move to a cell that still stops that threat
		var i = 0;
		repeat (ds_list_size(_options))
		{
			if (findArrayInList(_options[| i], _blocking)!=-1) ds_list_add(argument0, _options[| i]);
			i++;
		}
	}
	else
	{
		//all possible moves are good
		ds_list_copy(argument0, _options);
	}
}