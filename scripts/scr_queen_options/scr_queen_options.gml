///@func scr_queen_options
///@arg list

var _x = x,
	_y = y,
	_team = team,
	_steps =	[	-1,1,		0,1,		1,1,
					-1,0,					1,0,
					-1,-1,		0,-1,		1,-1	];
var i = 0;
repeat(array_length_1d(_steps)/2)
{
	var _xx = _x,
		_yy = _y,
		_step_x = _steps[i*2],
		_step_y = _steps[i*2+1],
		_exitLoop = false,
		_blocked = noone;
	do
	{
		_xx += _step_x;
		_yy += _step_y;
		var _valid = scr_validType(_xx, _yy, _team);
		switch _valid
		{
			case valid.false:
				_exitLoop = true;
				break;
			case valid.empty:
				if (_blocked==noone) ds_list_add(argument0, [_xx, _yy]);
				break;
			case valid.friend:
				if (_blocked==noone)
				{
					var _piece = scr_getPiece(_xx, _yy);
					_piece.threatened = true;
				}
				_exitLoop = true;
				break;
			case valid.opponent:
				if (_blocked==noone)
				{
					ds_list_add(argument0, [_xx, _yy]);
					_blocked = scr_getPiece(_xx, _yy);
				}
				else
				{
					_exitLoop = true;
				}
				break;
			case valid.opponent_king:
				if (_blocked==noone)
				{
					ds_list_add(argument0, [_xx, _yy]);
					var _directThreat = ds_list_create();
					var _list = (_team==team.black) ? LOGIC.black_king_threatening : LOGIC.white_king_threatening;
					var _xxx = _xx,
						_yyy = _yy;
					do
					{
						_xxx -= _step_x;
						_yyy -= _step_y;
						ds_list_add(_directThreat, [_xxx, _yyy]);
					} until (_xxx==_x && _yyy==_y);
					ds_list_add(_list, _directThreat);
					
					_xxx = _xx;
					_yyy = _yy;
					_blocked = scr_getPiece(_xx, _yy); //which is the king
					var _blockingList = _blocked.blocking; //king blocking list is used ro rule out king options
					do
					{
						//so entries are for steps the king blocks.
						//the king cell itself is covered in options list
						_xxx += _step_x;
						_yyy += _step_y;
						var _blockingValid = scr_validType(_xxx, _yyy, _team);
						switch _blockingValid
						{
							case valid.opponent:
							case valid.false:
							case valid.opponent_king:
								_exitLoop = true;
								break;
							case valid.empty:
								ds_list_add(_blockingList, [_xxx, _yyy]);
								break;
							case valid.friend:
								ds_list_add(_blockingList, [_xxx, _yyy]);
								_exitLoop = true;
								break;
						}
					}until(_exitLoop)
						
				}
				else
				{
					var _blockingList = _blocked.blocking;
					var _xxx = _xx,
						_yyy = _yy;
					do
					{
						//so the first entry would be one step before the king
						_xxx -= _step_x;
						_yyy -= _step_y;
						ds_list_add(_blockingList, [_xxx, _yyy]);
					} until (_xxx==_x && _yyy==_y); //so the last entry would be the threatening piece itself
				}
				_exitLoop = true;
				break;
			}	
	} until (_exitLoop);
	i++;
}