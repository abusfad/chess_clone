///@func scr_removePiece(id)
///@arg id

with argument0
{
	global.board[# x, y] = noone;
	var _teamList = (team==team.black) ? LOGIC.black_pieces : LOGIC.white_pieces;
	ds_list_delete(_teamList, ds_list_find_index(_teamList, argument0));
	disabled = true;
}