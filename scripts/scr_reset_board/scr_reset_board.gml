var _white_pieces = white_pieces;
var _black_pieces = black_pieces;
ds_list_clear(_black_pieces);
ds_list_clear(_white_pieces);
obj_piece.disabled = false;
ds_grid_clear(global.board, noone);
with (obj_piece)
{
	if (is_crowned_pawn) instance_destroy();
	else
	{
		x = xstart;
		y = ystart;
	
		ds_list_add((team==team.black) ? _black_pieces : _white_pieces, id);
		global.board[# x, y] = id;
	}
}

var _black_castling = black_castling,
	_white_castling = white_castling;
_black_castling[? castling.moved_king] = false;
_black_castling[? castling.moved_left] = false;
_black_castling[? castling.moved_right] = false;
_white_castling[? castling.moved_king] = false;
_white_castling[? castling.moved_left] = false;
_white_castling[? castling.moved_right] = false;

focus = noone;
turn = team.white;
check = false;
black_pawnDouble = -2;
white_pawnDouble = -2;
scr_update_options();
scr_reset_board_tiles();