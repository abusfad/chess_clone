with LOGIC
{
	//button_active = 1;  //not sure is neccessary. Used originally to solve a bug which I think fixing the while
	//loops in logic begin step solved more propely.
	
	var _options = button_options;
	ds_list_clear(_options);
	
	with (obj_button)
	{
		var _size = ds_list_size(_options),
			_size1 = _size-1,
			_y = y,
			_x = x;
		//iterate through the list and add yourself after the first button lower than you
		//or the same height as you but to the left or at the end of the current list
		for (var i=0;i<_size;i++)
		{
			var _button = _options[| i];
			if ((_button.y>_y) || (_button.y==_y && _button.x<=_x) || (i==_size1))
			{
				ds_list_insert(_options, i, id)
				break;
			}
		}
		//old method which might be a nanosecond more efficient but harder to read
		////if didn't add to the list in the loop add yourself now
		//if (ds_list_size(_options)==_size)
		//{
		//	ds_list_add(_options, id);
		//}
	}
}