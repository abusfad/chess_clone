///@func scr_saveBoard(final)
///@arg final

var _storedBoard = ds_map_create();
var _store;
	
//creates a list within a list and stores the top one in the map
_store = GridToLists(global.board);
ds_map_add_list(_storedBoard, "board", _store);

_store = ds_list_create();
ds_list_copy(_store, white_options);
ds_map_add_list(_storedBoard, "white_options", _store);

_store = ds_list_create();
ds_list_copy(_store, black_options);
ds_map_add_list(_storedBoard, "black_options", _store);
	
if (argument0)
{
	_store = ds_list_create();
	ds_list_copy(_store, white_options_final);
	ds_map_add_list(_storedBoard, "white_options_final", _store);

	_store = ds_list_create();
	ds_list_copy(_store, black_options_final);
	ds_map_add_list(_storedBoard, "black_options_final", _store);
}
	
var _pieces_list = ds_list_create();
var _store;
with (obj_piece)
{
	_store = ds_map_create();
	_store[? "id"] = id;
		
	//each option entry was an array and will turn to list in json
	var _copied_list = ds_list_create();
	ds_list_copy(_copied_list, options);
	ds_map_add_list(_store, "options", _copied_list);
		
	ds_list_add_map(_pieces_list, _store);
}
ds_map_add_list(_storedBoard, "pieces_list", _pieces_list);
	
var _data = json_encode(_storedBoard);
ds_map_destroy(_storedBoard);
return _data;
