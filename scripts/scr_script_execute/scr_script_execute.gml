///@func scr_script_execute(script, arguments_array)
///@desc scr_script_execute(script, arguments_array)
///@arg script
///@arg arguments_array

var _script = argument0,
	_args = argument1;

switch (array_length_1d(_args))
{
	case 0 : script_execute(_script); break;
	case 1 : script_execute(_script, _args[0]); break;
	case 2 : script_execute(_script, _args[0], _args[1]); break;
	case 3 : script_execute(_script, _args[0], _args[1], _args[2]); break;
	case 4 : script_execute(_script, _args[0], _args[1], _args[2], _args[3]); break;
	case 5 : script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4]); break;
	case 6 : script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5]); break;
	case 7 : script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6]); break;
	case 8 : script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7]); break;
	case 9 : script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8]); break;
	case 10: script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8], _args[9]); break;
	case 11: script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8], _args[9], _args[10]); break;
	case 12: script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8], _args[9], _args[10], _args[11]); break;
	case 13: script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8], _args[9], _args[10], _args[11], _args[12]); break;
	case 14: script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8], _args[9], _args[10], _args[11], _args[12], _args[13]); break;
	case 15: script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8], _args[9], _args[10], _args[11], _args[12], _args[13], _args[14]); break;
	case 16: script_execute(_script, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8], _args[9], _args[10], _args[11], _args[12], _args[13], _args[14], _args[15]); break;
	default: show_error("scr_script_execute got too many arguments", true); break;
}

