if (event_type==ev_draw)
{
	shader_set(shd_heatmap);
	shader_set_uniform_f_array(heatmap_board, heatmap_values);
	shader_set_uniform_f(heatmap_turn, global.turns);
}