///@func scr_tileColor(x, y)
///@arg x
///@arg y

return ((argument0%2)==0) ?
							(((argument1%2)==0) ? tile.white : tile.black)
							: (((argument1%2)==0) ? tile.black : tile.white);