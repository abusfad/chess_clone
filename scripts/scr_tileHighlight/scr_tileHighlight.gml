///@func scr_tileHighlight(tile, opponent)
///@arg tile
///@arg validType

switch argument1
{
	case valid.opponent:
		return tile.red; break;
	case valid.empty:
		return (argument0==tile.black) ? tile.black_h : tile.white_h;
		break;
}	