///@func scr_timer_init(seconds, function, x, y, width, height, xalign, yalign, text_xalign, text_yalign, style)
///@desc scr_timer_init(seconds, function, x, y, width, height, xalign, yalign, text_xalign, text_yalign, style)
///@arg seconds
///@arg function
///@arg x
///@arg y
///@arg width
///@arg height
///@arg xalign
///@arg yalign
///@arg text_xalign
///@arg text_yalign
///@arg style
///@arg fractions

enum timepart
{
	fractions,
	seconds
}

var _timer = instance_create_layer(argument[2], argument[3], LAYER_UI, obj_timer);
with (_timer)
{
	time[timepart.seconds] = argument[0]&~0;
	time[timepart.fractions] = frac(argument[0])*100;
	function = argument[1];
	
	scr_uiBoxText_settings(argument[4], argument[5], argument[6], argument[7],
							"", argument[9], argument[9], argument[10]);
	
	use_fractions = argument[11];
	
	if (use_fractions) alarm[0] = room_speed/20;
	else alarm[0] = room_speed;
}
return _timer;