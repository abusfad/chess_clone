///@func scr_uiBoxText_settings(width, height, xalign, yalign, text, text_xalign, text_yalign, style)
///@desc scr_uiBoxText_settings(width, height, xalign, yalign, text, text_xalign, text_yalign, style)
///@arg width
///@arg height
///@arg xalign
///@arg yalign
///@arg text
///@arg text_xalign
///@arg text_yalign
///@arg style

text = argument4;
text_xalign = argument5;
text_yalign = argument6;

scr_uiBox_settings(argument0, argument1, argument2, argument3, argument7);	

scr_uiBoxText_textPos();

scr_uiBoxText_style(argument7);