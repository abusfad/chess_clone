///@func scr_uiBox_styleApply(style)
///@desc scr_uiBox_styleApply(style)
///@arg style

switch argument0
{
	case uiStyles.old_software:
		text_color = c_black;
		font = fnt_basic;
		break;
	case uiStyles.garden:
		text_color = c_lime;
		font = fnt_basic;
		break;	
}