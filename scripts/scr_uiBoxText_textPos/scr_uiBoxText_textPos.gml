///@func scr_uiBoxText_textPos
///@desc update variables for drawing the text


var _offset = outline+TEXT_OFFSET,
	_width = width-2*_offset,
	_height = height-2*_offset,
	_text = text;

//adjust draw position according to text alignment

switch(text_xalign)
{
	case fa_left: text_x = topLeft_x+_offset; break;
	case fa_center: text_x = topLeft_x+_offset+_width/2; break;
	case fa_right: text_x = topLeft_x+_offset+_width; break;
}
switch(text_yalign)
{
	case fa_top: text_y = topLeft_y+_offset; break;
	case fa_center: text_y = topLeft_y+_offset+_height/2; break;
	case fa_right: text_y = topLeft_y+_offset+_height; break;
}