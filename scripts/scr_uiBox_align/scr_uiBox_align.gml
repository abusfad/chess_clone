///@func scr_uiBox_alignApply(xalign, yalign)
///@desc scr_uiBox_alignApply(xalign, yalign)
///@arg xalign
///@arg yalign

switch argument0
{
	case fa_left: topLeft_x = x; break;
	case fa_center: topLeft_x = x-width/2; break;
	case fa_right: topLeft_x = x-width; break;
}
switch argument1
{
	case fa_top: topLeft_y = y; break;
	case fa_center: topLeft_y = y-height/2; break;
	case fa_bottom: topLeft_y = y-height; break;
}