///@func scr_uiBox_init(x, y, width, height, xalign, yalign, style)
///@arg x
///@arg y
///@arg width
///@arg height
///@arg xalign
///@arg yalign
///@arg style

var _uiBox = instance_create_layer(argument[0], argument[1], LAYER_UI, obj_uiBox);
with (_uiBox)
{
	scr_uiBox_settings(argument[2], argument[3], argument[4], argument[5], argument[6]);
}
return _uiBox;