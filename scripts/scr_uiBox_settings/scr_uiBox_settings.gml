///@func scr_uiBox_settings(width, height, xalign, yalign, style)
///@arg width
///@arg height
///@arg xalign
///@arg yalign
///@arg style

width = argument0;
height = argument1;

scr_uiBox_align(argument2, argument3);

scr_uiBox_style(argument4);