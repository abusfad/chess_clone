///@func scr_uiBox_styleApply(style)
///@desc scr_uiBox_styleApply(style)
///@arg style
enum uiStyles
{
	old_software,
	garden
}


switch argument0
{
	case uiStyles.old_software:
		outline = 8;
		outline_color = c_black;
		bg_color = c_gray;
		alpha = 0.5;
		break;
	case uiStyles.garden:
		outline = 6;
		outline_color = c_orange;
		bg_color = c_yellow;
		alpha = 0.5;
		break;	
}