turn++;

scr_update_options();

//board_memory[| turn] = scr_saveBoard(true);

focus = noone;

scr_reset_board_tiles();


//game ending
var _turnTeam = turn%2;
var _finalOptions = (_turnTeam==team.black) ? black_options_final : white_options_final;

if (scr_win_impossible())
{
	//show_message("stalemate");
	global.stalemates_lack++;
	global.stalemates++;
	global.games++;
	scr_restart_gameMode();
}
else if (ds_list_empty(_finalOptions))
{
	if (!check)
	{
		global.stalemates_impasse++;
		global.stalemates++;
		global.games++;
		//show_message("stalemate");
	}
	else
	{
		if (_turnTeam==team.black)
		{
			global.whiteWins++;
			global.games++;
			//var _teamText = (_turnTeam==team.black) ? "white" : "black";
		}
		else
		{
			global.blackWins++;
			global.games++;
		}
		//show_message(_teamText+" won");
	}
	scr_restart_gameMode();
}

switch game_mode
{
	case game_modes.pvc:
		if (_turnTeam != player_team && game_state == game_states.normal) scr_computer_turn();
		break;
}