///@func scr_update_options()

var _white_options = white_options,
	_black_options = black_options;
	
ds_list_clear(_white_options);
ds_list_clear(_black_options);

var _team = turn%2,
	_team_pieces = (_team==team.black) ? black_pieces : white_pieces,
	_opponent_threat = (_team==team.black) ? white_king_threatening : black_king_threatening;

obj_piece.threatened = false;

var i = 0,
	_team_size = ds_list_size(_team_pieces);
repeat(_team_size)
{
	with (_team_pieces[| i])
	{
		ds_list_clear(blocking);
	}
	i++;
}

var i = 0;
repeat(ds_list_size(_opponent_threat))
{
	ds_list_destroy(_opponent_threat[| i]);
	i++;
}
ds_list_clear(_opponent_threat);

with (obj_piece)
{
	if (!disabled)
	{
		ds_list_clear(options);
		script_execute(options_script, options);
		var _overall_list = (team==team.black) ? _black_options : _white_options;
		ListAppend(_overall_list, options);
	}
}

var _overall_list = (_team==team.black) ? black_options_final : white_options_final;
ds_list_clear(_overall_list);
var i = 0;
repeat(_team_size)
{
	with (_team_pieces[| i])
	{
		var _options_final = options_final;
		ds_list_clear(_options_final)
		script_execute(options_final_script, _options_final, _opponent_threat);
		ListAppend(_overall_list, _options_final);
	}
	i++;
}