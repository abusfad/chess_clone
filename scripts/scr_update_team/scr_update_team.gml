///@func scr_update_team(piece, team)
///@arg piece
///@arg team


with (argument0)
{
	team = argument1;
	
	switch object_index
	{
		case obj_rook:
			sprite_index = (argument1==team.black) ? spr_rook_black : spr_rook_white;
			break;
		case obj_knight:
			sprite_index = (argument1==team.black) ? spr_knight_black : spr_knight_white;
			break;
		case obj_bishop:
			sprite_index = (argument1==team.black) ? spr_bishop_black : spr_bishop_white;
			break;
		case obj_queen:
			sprite_index = (argument1==team.black) ? spr_queen_black : spr_queen_white;
			break;
		case obj_king:
			sprite_index = (argument1==team.black) ? spr_king_black : spr_king_white;
			break;
		case obj_pawn:
			sprite_index = (argument1==team.black) ? spr_pawn_black : spr_pawn_white;
			break;
	}
}