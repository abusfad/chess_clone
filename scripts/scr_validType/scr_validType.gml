///@func scr_validType(x, y, team)
///@arg x
///@arg y
///@arg team

gml_pragma("forceinline");

enum valid
{
	false,
	friend,
	empty,
	opponent,
	opponent_king
}

var _x = argument0, _y = argument1;
if (!scr_inBounds(_x, _y)) return valid.false;
else
{
	var _piece = scr_getPiece(_x, _y);
	if (_piece==noone) return valid.empty;
	else if (_piece.team==argument2) return valid.friend;
	else if (_piece.object_index==obj_king) return valid.opponent_king;
	else return valid.opponent;
}