///@func scr_win_impossible
/*
King + Bishop + Knight
King + Bishop + Bishop
King + Rook
King + Queen
King + Knight + Knight + Knight
*/

var _white_pieces = white_pieces,
	_black_pieces = black_pieces;

var _knights = 0, _bishops = 0;
var i = 0;
repeat(ds_list_size(_white_pieces))
{
	var _piece = _white_pieces[| i];

	switch _piece.object_index
	{
		case obj_pawn:
			return false;
			break;
		case obj_rook:
			return false;
			break;
		case obj_knight:
			_knights++;
			break;
		case obj_bishop:
			_bishops++;
			break;
		case obj_queen:
			return false;
			break;
	}
	if (_knights>=3 || (_knights>=1&&_bishops>=1) || _bishops>=2)
	{
		return false;
	}
	i++;
}

var _knights = 0, _bishops = 0;
var i = 0;
repeat(ds_list_size(_black_pieces))
{
	var _piece = _black_pieces[| i];
	switch _piece.object_index
	{
		case obj_pawn:
			return false;
			break;
		case obj_rook:
			return false;
			break;
		case obj_knight:
			_knights++;
			break;
		case obj_bishop:
			_bishops++;
			break;
		case obj_queen:
			return false;
			break;
	}
	if (_knights>=3 || (_knights>=1&&_bishops>=1	) || _bishops>=2)
	{
		return false;
	}
	i++;
}
	
return true;