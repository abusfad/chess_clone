varying vec3 v_vPosition;
varying vec4 v_vColour;

uniform float board[64];
uniform float turn;
const float fullTurns = 1000.0;
const float radius = 512.0;
const vec4 heatCol6 = vec4(1.0, 0.0, 0.0, 1.0);
const vec4 heatCol5 = vec4(1.0, 1.0, 0.0, 1.0);
const vec4 heatCol4 = vec4(0.0, 1.0, 0.0, 1.0);
const vec4 heatCol3 = vec4(0.0, 1.0, 1.0, 1.0);
const vec4 heatCol2 = vec4(0.0, 0.0, 1.0, 1.0);
const vec4 heatCol1 = vec4(0.0, 0.0, 0.0, 1.0);
const float thresh1 = 0.2;
const float thresh2 = 0.4;
const float thresh3 = 0.6;
const float thresh4 = 0.8;
const float thresh5 = 1.0;
const float threshDif = 0.2;

vec4 ColorThreshold(float heat)
{	
	if (heat<thresh1) {return mix(heatCol1, heatCol2, (heat)/threshDif); }
	if (heat<thresh2) {return mix(heatCol2, heatCol3, (heat-thresh1)/threshDif); }
	if (heat<thresh3) {return mix(heatCol3, heatCol4, (heat-thresh2)/threshDif); }
	if (heat<thresh4) {return mix(heatCol4, heatCol5, (heat-thresh3)/threshDif); }
	return mix(heatCol5, heatCol6, (heat-thresh4)/threshDif);
}

void main()
{
	float highest_val = 10.0;
	for (int i = 0; i < 64; i++)
	{
		if (board[i]>highest_val) { highest_val = board[i];	}
	}
	
	vec2 board_pos = v_vPosition.xy;
	
	float board_x = floor(board_pos.x/256.0);
	float board_y = floor(board_pos.y/256.0);
	
	int point1 = int(board_x+board_y*8.0);
	int point2 = int(board_x-1.0+board_y*8.0);
	int point3 = int(board_x+1.0+board_y*8.0);
	int point4 = int(board_x+(board_y-1.0)*8.0);
	int point5 = int(board_x+(board_y+1.0)*8.0);
	int point6 = int(board_x-1.0+(board_y-1.0)*8.0);
	int point7 = int(board_x+1.0+(board_y-1.0)*8.0);
	int point8 = int(board_x-1.0+(board_y+1.0)*8.0);
	int point9 = int(board_x+1.0+(board_y+1.0)*8.0);

	float board_point_x = board_x*258.0 + 128.0;
	float board_point_y = board_y*258.0 + 128.0;

	float dist1 = distance(board_pos, vec2(board_point_x, board_point_y));
	float dist2 = distance(board_pos, vec2(board_point_x-256.0, board_point_y));
	float dist3 = distance(board_pos, vec2(board_point_x+256.0, board_point_y));
	float dist4 = distance(board_pos, vec2(board_point_x, board_point_y-256.0));
	float dist5 = distance(board_pos, vec2(board_point_x, board_point_y+256.0));
	float dist6 = distance(board_pos, vec2(board_point_x-256.0, board_point_y-256.0));
	float dist7 = distance(board_pos, vec2(board_point_x+256.0, board_point_y-256.0));
	float dist8 = distance(board_pos, vec2(board_point_x-256.0, board_point_y+256.0));
	float dist9 = distance(board_pos, vec2(board_point_x+256.0, board_point_y+256.0));
	
	float board_heat_1 = (board[point1] / highest_val) * sqrt(1.0-dist1/radius);
	float board_heat_2 = (board[point2] / highest_val) * sqrt(1.0-dist2/radius);
	float board_heat_3 = (board[point3] / highest_val) * sqrt(1.0-dist3/radius);
	float board_heat_4 = (board[point4] / highest_val) * sqrt(1.0-dist4/radius);
	float board_heat_5 = (board[point5] / highest_val) * sqrt(1.0-dist5/radius);
	float board_heat_6 = (board[point6] / highest_val) * sqrt(1.0-dist6/radius);
	float board_heat_7 = (board[point7] / highest_val) * sqrt(1.0-dist7/radius);
	float board_heat_8 = (board[point8] / highest_val) * sqrt(1.0-dist8/radius);
	float board_heat_9 = (board[point9] / highest_val) * sqrt(1.0-dist9/radius);
	
	float heat = max(board_heat_1, board_heat_2);
	heat = max(heat, board_heat_3);
	heat = max(heat, board_heat_4);
	heat = max(heat, board_heat_5);
	heat = max(heat, board_heat_6);
	heat = max(heat, board_heat_7);
	heat = max(heat, board_heat_8);
	heat = max(heat, board_heat_9);
	//heat = min(heat, heat*turn/fullTurns);
	
	gl_FragColor = ColorThreshold(heat);
	
    //gl_FragColor = heatCol5;
}