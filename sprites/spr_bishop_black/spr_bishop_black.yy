{
    "id": "444b1b26-2214-4ead-8cb9-10727ae24a38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bishop_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccfaba32-278d-4112-94be-e7d295936dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "444b1b26-2214-4ead-8cb9-10727ae24a38",
            "compositeImage": {
                "id": "9c4c2e4f-9842-47cb-8690-7971883bb1d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccfaba32-278d-4112-94be-e7d295936dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aafe7eb6-2c88-4e26-8099-0345e905249b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccfaba32-278d-4112-94be-e7d295936dc7",
                    "LayerId": "cfd0da55-f647-4c4b-a390-2ebeaae32c28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "cfd0da55-f647-4c4b-a390-2ebeaae32c28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "444b1b26-2214-4ead-8cb9-10727ae24a38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}