{
    "id": "97763c1d-2f10-4b9f-a321-ba87a084ee6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bishop_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9438860a-0fe5-4c57-a6b8-9773e7d2767a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97763c1d-2f10-4b9f-a321-ba87a084ee6f",
            "compositeImage": {
                "id": "507f9c07-1d92-489b-afab-5dbc6715cb92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9438860a-0fe5-4c57-a6b8-9773e7d2767a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "557d0b92-262d-4ea5-9187-70bf0b4533f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9438860a-0fe5-4c57-a6b8-9773e7d2767a",
                    "LayerId": "286bd3bf-d17d-42dd-9761-d1f5d7da2290"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "286bd3bf-d17d-42dd-9761-d1f5d7da2290",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97763c1d-2f10-4b9f-a321-ba87a084ee6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}