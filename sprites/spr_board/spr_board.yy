{
    "id": "8abd321b-c71d-4bb3-b740-cc9e5e02eed2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_board",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 256,
    "bbox_right": 1791,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "487c4b9f-4539-4010-8ee7-c3634415e9ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8abd321b-c71d-4bb3-b740-cc9e5e02eed2",
            "compositeImage": {
                "id": "4ade6dae-fbff-448e-b31a-a52f6b39fbaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "487c4b9f-4539-4010-8ee7-c3634415e9ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59797685-9835-4ac7-9ce8-612a1838130b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487c4b9f-4539-4010-8ee7-c3634415e9ca",
                    "LayerId": "198557fb-56d5-4e80-89ff-5963d49b6ff3"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 256,
    "layers": [
        {
            "id": "198557fb-56d5-4e80-89ff-5963d49b6ff3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8abd321b-c71d-4bb3-b740-cc9e5e02eed2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1792,
    "xorig": 0,
    "yorig": 0
}