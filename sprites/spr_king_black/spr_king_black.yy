{
    "id": "c276704c-31bf-4db5-8356-0f206e07c3b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_king_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 1,
    "bbox_right": 255,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3f326dc-549e-480e-b77d-e8a93ffaf459",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c276704c-31bf-4db5-8356-0f206e07c3b7",
            "compositeImage": {
                "id": "0863fdb7-f49f-4b6b-9bbb-ad65038ee3da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3f326dc-549e-480e-b77d-e8a93ffaf459",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c8b2c9-9f35-4778-8738-2b0c44b8ce5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3f326dc-549e-480e-b77d-e8a93ffaf459",
                    "LayerId": "a51f33f4-9751-46a6-85fb-30ba83cfaa16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a51f33f4-9751-46a6-85fb-30ba83cfaa16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c276704c-31bf-4db5-8356-0f206e07c3b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}