{
    "id": "ca63d635-8d96-46de-8625-c82b012beffd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 251,
    "bbox_left": 4,
    "bbox_right": 252,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "235afb09-375d-4bde-b445-392d6d874e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca63d635-8d96-46de-8625-c82b012beffd",
            "compositeImage": {
                "id": "64814aea-5cb7-4234-b713-97de86107126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235afb09-375d-4bde-b445-392d6d874e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed594f3-18bf-4150-86ab-5b7316152fd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235afb09-375d-4bde-b445-392d6d874e91",
                    "LayerId": "5181daf1-a931-4779-8c97-aac09bac711d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "5181daf1-a931-4779-8c97-aac09bac711d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca63d635-8d96-46de-8625-c82b012beffd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}