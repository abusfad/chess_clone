{
    "id": "963b1cff-599f-4e93-8a16-cf5a7517001e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 4,
    "bbox_right": 252,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b59a1a0b-0ca7-4a43-a0af-2bd61e9b97ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963b1cff-599f-4e93-8a16-cf5a7517001e",
            "compositeImage": {
                "id": "84e6c5b9-aa0c-4684-af48-fa57936e5a14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b59a1a0b-0ca7-4a43-a0af-2bd61e9b97ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d7fe424-d762-488c-a2b4-30a5d1a84eb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b59a1a0b-0ca7-4a43-a0af-2bd61e9b97ec",
                    "LayerId": "b02d5d36-187b-47d4-8bc6-bd8f8fc44732"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b02d5d36-187b-47d4-8bc6-bd8f8fc44732",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "963b1cff-599f-4e93-8a16-cf5a7517001e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}