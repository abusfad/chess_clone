{
    "id": "285cfe9f-a2e2-4e0a-b308-93df2fa799ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_memory_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e646a2c-fa9f-4166-9ec7-54dba5a33e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285cfe9f-a2e2-4e0a-b308-93df2fa799ac",
            "compositeImage": {
                "id": "d6eefd8c-0447-49b5-afac-c9c8031c3a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e646a2c-fa9f-4166-9ec7-54dba5a33e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d68fd99-c40c-4193-b1c0-b012e7352f32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e646a2c-fa9f-4166-9ec7-54dba5a33e30",
                    "LayerId": "c1be6f91-3310-4260-9f76-0b7eacbfdf4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c1be6f91-3310-4260-9f76-0b7eacbfdf4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "285cfe9f-a2e2-4e0a-b308-93df2fa799ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}