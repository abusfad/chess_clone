{
    "id": "947976f4-2618-48a6-bd6d-908c653a1bbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pawn_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 36,
    "bbox_right": 218,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66d774b7-5a2a-4065-b1a2-d0b1d521b345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "947976f4-2618-48a6-bd6d-908c653a1bbd",
            "compositeImage": {
                "id": "6bb3df22-e609-4e59-868b-3757f975df94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66d774b7-5a2a-4065-b1a2-d0b1d521b345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0123d87d-5ea8-4721-ad07-a54f91203685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66d774b7-5a2a-4065-b1a2-d0b1d521b345",
                    "LayerId": "d4552645-900c-4f22-a752-064bcf4e660d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d4552645-900c-4f22-a752-064bcf4e660d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "947976f4-2618-48a6-bd6d-908c653a1bbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}