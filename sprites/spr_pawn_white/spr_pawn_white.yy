{
    "id": "5d6dc988-7d9b-4b7c-8987-f2efde8ff8c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pawn_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 37,
    "bbox_right": 219,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37553152-83c0-4e7d-9bf4-7d33a8431b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d6dc988-7d9b-4b7c-8987-f2efde8ff8c8",
            "compositeImage": {
                "id": "bd9376c4-4d76-4072-b9bf-7d1f98ef3d6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37553152-83c0-4e7d-9bf4-7d33a8431b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b574107-b830-4545-9668-2a6b9bf6c8b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37553152-83c0-4e7d-9bf4-7d33a8431b21",
                    "LayerId": "70a05410-3e46-4834-a80b-fbe4067ba556"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "70a05410-3e46-4834-a80b-fbe4067ba556",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d6dc988-7d9b-4b7c-8987-f2efde8ff8c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}