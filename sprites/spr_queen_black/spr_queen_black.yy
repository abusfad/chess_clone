{
    "id": "0236ec01-9a65-487e-b442-3af305414136",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_queen_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce78e519-bebf-4eac-af5a-0ace30f42690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0236ec01-9a65-487e-b442-3af305414136",
            "compositeImage": {
                "id": "6dd2729c-7b2b-4705-b331-542cdf540bac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce78e519-bebf-4eac-af5a-0ace30f42690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c150b30-b7d5-495f-a5d0-48700951d5e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce78e519-bebf-4eac-af5a-0ace30f42690",
                    "LayerId": "9161a48a-55a8-4d98-9be2-fb38a8f92521"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9161a48a-55a8-4d98-9be2-fb38a8f92521",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0236ec01-9a65-487e-b442-3af305414136",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}