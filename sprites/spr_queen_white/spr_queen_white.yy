{
    "id": "fbaea966-49fc-4ed1-865b-357f45a1bf00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_queen_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3aa866f1-177e-4a4c-9a84-9d4400a8b783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbaea966-49fc-4ed1-865b-357f45a1bf00",
            "compositeImage": {
                "id": "48903a9d-c154-44c8-b560-3e3aa567d0d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aa866f1-177e-4a4c-9a84-9d4400a8b783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33a014d-bb46-4fee-8e0b-eef3a099fa68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aa866f1-177e-4a4c-9a84-9d4400a8b783",
                    "LayerId": "5bc43ced-32cc-46c3-9297-ef0fa86940e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "5bc43ced-32cc-46c3-9297-ef0fa86940e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbaea966-49fc-4ed1-865b-357f45a1bf00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}