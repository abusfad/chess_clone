{
    "id": "7ca2a74c-5563-4f39-b6b6-7039439d4a51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rook_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 244,
    "bbox_left": 22,
    "bbox_right": 233,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c799c7d-3775-4b4b-8116-e3eb0c6d316c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ca2a74c-5563-4f39-b6b6-7039439d4a51",
            "compositeImage": {
                "id": "5353f7e7-c9b1-40c8-b0db-db85d04f1756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c799c7d-3775-4b4b-8116-e3eb0c6d316c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54b20f15-e0d5-4779-95af-5ddb4ac1b6a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c799c7d-3775-4b4b-8116-e3eb0c6d316c",
                    "LayerId": "46737086-ad6d-4e3d-a318-3826810a3501"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "46737086-ad6d-4e3d-a318-3826810a3501",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ca2a74c-5563-4f39-b6b6-7039439d4a51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}