{
    "id": "bd65d601-b742-4f4b-a665-a0497b750872",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rook_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 244,
    "bbox_left": 22,
    "bbox_right": 233,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1ff67ca-a423-420d-a109-d8a94d4e7a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd65d601-b742-4f4b-a665-a0497b750872",
            "compositeImage": {
                "id": "85080986-e080-47a5-a4dd-1cf738ed4079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1ff67ca-a423-420d-a109-d8a94d4e7a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da9864d-02a9-4991-9d46-ad140247eac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1ff67ca-a423-420d-a109-d8a94d4e7a65",
                    "LayerId": "ac8f2f0c-e60d-4f32-ac35-9288e1d82bf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ac8f2f0c-e60d-4f32-ac35-9288e1d82bf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd65d601-b742-4f4b-a665-a0497b750872",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}