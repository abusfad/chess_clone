{
    "id": "2ccadca8-9d59-4c93-8a95-a515e14c22ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45bde0c5-5edc-4e39-be65-21ac708abf77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ccadca8-9d59-4c93-8a95-a515e14c22ad",
            "compositeImage": {
                "id": "9a4bbc0f-9b09-4e3e-9bf3-51bb7dd72bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45bde0c5-5edc-4e39-be65-21ac708abf77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c214fe8-e97c-4913-8c21-627f31da0f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45bde0c5-5edc-4e39-be65-21ac708abf77",
                    "LayerId": "99f00179-37f3-45ee-aa52-3a8100a18aae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "99f00179-37f3-45ee-aa52-3a8100a18aae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ccadca8-9d59-4c93-8a95-a515e14c22ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}